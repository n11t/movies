<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieOMDBAdapterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class N11tMovieOMDBAdapterBundle extends Bundle
{
}
