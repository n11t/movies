<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieOMDBAdapterBundle;

use Jleagle\Imdb\Exceptions\ImdbException;
use Jleagle\Imdb\Imdb;
use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;
use N11t\Bundle\MovieAdapterBundle\Exception\MovieNotFoundException;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieOMDBAdapterBundle\Struct\OMDBMovie;

class OMDBMovieApiAdapter implements MovieApiAdapterInterface
{

    public function __construct(string $apiKey)
    {
        Imdb::setApiKey($apiKey);
    }

    public function getName(): string
    {
        return 'omdb';
    }

    /**
     * {@inheritDoc}
     */
    public function findByImdbId(string $imdbId): MovieInterface
    {
        try {
            $movie = Imdb::retrieve($imdbId, Imdb::TYPE_MOVIE, null, false, true);

            return new OMDBMovie($movie);
        } catch (ImdbException $exception) {
            throw new MovieNotFoundException($imdbId);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(string $query): array
    {
        try {
            $results = Imdb::search($query, Imdb::TYPE_MOVIE);
        } catch (ImdbException $exception) {
            return [];
        }

        $movies = [];
        foreach ($results as $result) {
            try {
                $movies[] = $this->findByImdbId($result->imdbId);
            } catch (MovieNotFoundException $exn) {
                // no-op
            }
        }

        return $movies;
    }
}
