<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieOMDBAdapterBundle\Struct;

use Jleagle\Imdb\Responses\Movie;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;

class OMDBMovie implements MovieInterface
{

    /**
     * @var Movie
     */
    private $omdbMovie;

    public function __construct(Movie $omdbMovie)
    {
        $this->omdbMovie = $omdbMovie;
    }

    public function getImdbId(): string
    {
        return $this->omdbMovie->imdbId;
    }

    public function getTitle(): string
    {
        return $this->omdbMovie->title;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        $released = $this->omdbMovie->released;
        if (!is_string($released) || $released == 'N/A') {
            return null;
        }

        return new \DateTime($released);
    }

    public function getDescription(): string
    {
        $plot = $this->omdbMovie->plot;
        if (!is_string($plot) || $plot == 'N/A') {
            return '';
        }

        return $plot;
    }

    public function getLength(): int
    {
        return (int)$this->omdbMovie->runtime;
    }

    public function getImageUrl(): ?string
    {
        $poster = $this->omdbMovie->poster;
        if (!is_string($poster) || $poster == 'N/A') {
            return null;
        }

        return $poster;
    }

    public function getGenres(): array
    {
        return explode(',', $this->omdbMovie->genre);
    }

    public function getActors(): array
    {
        return explode(',', $this->omdbMovie->actors);
    }
}
