<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Input;

interface ListInputInterface
{

    public function getQuery(): ?string;

    public function getOrderBy(): array;

    public function getLimit(): ?int;

    public function getPage(): ?int;
}
