<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Input;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;

interface WatchlistListInputInterface extends ListInputInterface
{

    /**
     * @return int[]|null
     */
    public function getGenres(): ?array;

    /**
     * @return int[]|null
     */
    public function getActors(): ?array;
}
