<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Input;

interface WatchlistAddInputInterface
{

    /**
     * @return int[]
     */
    public function getIds(): array;
}
