<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use N11t\Bundle\MovieBundle\Repository\WatchlistRepository;

class WatchlistListGateway implements WatchlistListGatewayInterface
{

    /**
     * @var WatchlistRepository
     */
    private $watchlistRepository;

    public function __construct(WatchlistRepository $watchlistRepository)
    {
        $this->watchlistRepository = $watchlistRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(?string $query, ?array $genres, ?array $actors, array $orderBy, ?int $limit, int $offset): array
    {
        $qb = $this->getQueryBuilder($query, $genres, $actors);

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }
        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }
        foreach ($orderBy as $sort => $order) {
            $qb->addOrderBy('m.' . $sort, $order);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * {@inheritDoc}
     * @throws NonUniqueResultException
     */
    public function countBy(?string $query, ?array $genres, ?array $actors): int
    {
        $qb = $this->getQueryBuilder($query, $genres, $actors);

        $qb->select('count(m.id)');

        $query = $qb->getQuery();

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @return QueryBuilder
     */
    private function getQueryBuilder(?string $query, ?array $genres, ?array $actors): QueryBuilder
    {
        $qb = $this->watchlistRepository->createQueryBuilder('wl')
            ->join('wl.movie', 'm')
        ;

        if (is_string($query)) {
            $qb
                ->orWhere('m.imdbId LIKE :query')
                ->orWhere('m.title LIKE :query')
                ->orWhere('m.description LIKE :query')
                ->setParameter('query', "%$query%")
            ;
        }
        if (is_array($genres) && count($genres) > 0) {
            $qb
                ->join('m.genres', 'mg')
                ->andWhere($qb->expr()->in('mg.id', $genres))
            ;
        }
        if (is_array($actors) && count($actors) > 0) {
            $qb
                ->join('m.actors', 'ma')
                ->andWhere($qb->expr()->in('ma.id', $actors))
            ;
        }

        return $qb;
    }
}
