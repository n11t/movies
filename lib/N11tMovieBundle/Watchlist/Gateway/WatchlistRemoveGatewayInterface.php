<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Gateway;

use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;

interface WatchlistRemoveGatewayInterface
{

    public function find(int $id): ?WatchlistEntry;

    public function remove(WatchlistEntry $entry): void;
}
