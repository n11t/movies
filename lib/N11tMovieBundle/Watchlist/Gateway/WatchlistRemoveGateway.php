<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Gateway;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;
use N11t\Bundle\MovieBundle\Repository\WatchlistRepository;

class WatchlistRemoveGateway implements WatchlistRemoveGatewayInterface
{

    /**
     * @var WatchlistRepository
     */
    private $watchlistRepository;

    public function __construct(WatchlistRepository $watchlistRepository)
    {
        $this->watchlistRepository = $watchlistRepository;
    }

    public function find(int $id): ?WatchlistEntry
    {
        return $this->watchlistRepository->find($id);
    }

    /**
     * @param WatchlistEntry $entry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(WatchlistEntry $entry): void
    {
        $this->watchlistRepository->remove($entry);
    }
}
