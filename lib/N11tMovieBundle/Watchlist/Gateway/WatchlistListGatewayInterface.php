<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Gateway;

use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;

interface WatchlistListGatewayInterface
{

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @param array $orderBy
     * @param int|null $limit
     * @param int $offset
     * @return WatchlistEntry[]
     */
    public function findBy(?string $query, ?array $genres, ?array $actors, array $orderBy, ?int $limit, int $offset): array;

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @return int
     */
    public function countBy(?string $query, ?array $genres, ?array $actors): int;
}
