<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Gateway;

use N11t\Bundle\MovieBundle\Entity\Movie;

interface WatchlistAddGatewayInterface
{

    public function find(int $id): ?Movie;

    public function addToWatchlist(Movie $movie): void;

    public function isInWatchlist(Movie $movie): bool;
}
