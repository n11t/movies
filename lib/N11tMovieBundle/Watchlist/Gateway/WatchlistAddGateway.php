<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Gateway;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use N11t\Bundle\MovieBundle\Repository\WatchlistRepository;

class WatchlistAddGateway implements WatchlistAddGatewayInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var WatchlistRepository
     */
    private $watchlistRepository;

    public function __construct(MovieRepository $movieRepository, WatchlistRepository $watchlistRepository)
    {
        $this->movieRepository = $movieRepository;
        $this->watchlistRepository = $watchlistRepository;
    }

    public function find(int $id): ?Movie
    {
        return $this->movieRepository->find($id);
    }

    /**
     * @param Movie $movie
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addToWatchlist(Movie $movie): void
    {
        $entry = new WatchlistEntry($movie);

        $this->watchlistRepository->persist($entry);
    }

    public function isInWatchlist(Movie $movie): bool
    {
        return $this->watchlistRepository->hasMovie($movie);
    }
}
