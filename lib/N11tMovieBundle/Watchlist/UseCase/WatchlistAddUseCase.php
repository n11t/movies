<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\UseCase;

use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;
use N11t\Bundle\MovieBundle\Watchlist\Gateway\WatchlistAddGatewayInterface;
use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistAddInputInterface;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistAddOutputInterface;

class WatchlistAddUseCase
{
    /**
     * @var WatchlistAddGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        WatchlistAddGatewayInterface $gateway,
        MovieViewFactoryInterface $viewFactory
    ) {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(WatchlistAddInputInterface $input, WatchlistAddOutputInterface $output): void
    {
        $ids = $input->getIds();

        foreach ($ids as $id) {
            $movie = $this->findMovie((int)$id);
            if (!$movie instanceof Movie) {
                $output->movieNotFound($id);

                continue;
            }

            $view = $this->viewFactory->build($movie);
            if ($this->isMovieInWatchlist($movie)) {
                $output->movieIsInWatchlist($view);

                continue;
            }

            $this->addMovie($movie);

            $output->movieAdded($view);
        }
    }

    private function findMovie(int $id): ?Movie
    {
        return $this->gateway->find($id);
    }

    private function addMovie(Movie $movie): void
    {
        $this->gateway->addToWatchlist($movie);
    }

    private function isMovieInWatchlist(Movie $movie): bool
    {
        return $this->gateway->isInWatchlist($movie);
    }
}
