<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\UseCase;

use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;
use N11t\Bundle\MovieBundle\Watchlist\Gateway\WatchlistRemoveGatewayInterface;
use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistRemoveInputInterface;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistRemoveOutputInterface;

class WatchlistRemoveUseCase
{

    /**
     * @var WatchlistRemoveGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        WatchlistRemoveGatewayInterface $gateway,
        MovieViewFactoryInterface $viewFactory
    ) {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(WatchlistRemoveInputInterface $input, WatchlistRemoveOutputInterface $output): void
    {
        $ids = $input->getIds();

        foreach ($ids as $id) {
            $entry = $this->findEntry((int)$id);
            if (!$entry instanceof WatchlistEntry) {
                $output->entryNotFoundForId($id);

                continue;
            }

            $movie = $entry->getMovie();

            $this->removeEntry($entry);

            $view = $this->viewFactory->build($movie);

            $output->movieRemoved($view);
        }
    }

    private function findEntry(int $id): ?WatchlistEntry
    {
        return $this->gateway->find($id);
    }

    private function removeEntry(WatchlistEntry $entry): void
    {
        $this->gateway->remove($entry);
    }
}
