<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\UseCase;

use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;
use N11t\Bundle\MovieBundle\Watchlist\Gateway\WatchlistListGatewayInterface;
use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistListInputInterface;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistListOutputInterface;
use N11t\Bundle\MovieBundle\Watchlist\View\WatchlistViewFactoryInterface;

class WatchlistListUseCase
{
    /**
     * @var WatchlistListGatewayInterface
     */
    private $gateway;

    /**
     * @var WatchlistViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        WatchlistListGatewayInterface $gateway,
        WatchlistViewFactoryInterface $viewFactory)
    {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(WatchlistListInputInterface $input, WatchlistListOutputInterface $output): void
    {
        $count = $this->countBy($input);
        $output->setCount($count);


        $entries = $this->find($input);
        foreach ($entries as $entry) {
            $view = $this->viewFactory->build($entry);

            $output->addEntry($view);
        }
    }

    /**
     * @param WatchlistListInputInterface $input
     * @return WatchlistEntry[]
     */
    private function find(WatchlistListInputInterface $input): array
    {
        $query = $input->getQuery();
        $genres = $input->getGenres();
        $actors = $input->getActors();
        $orderBy = $input->getOrderBy();
        $limit = $input->getLimit();
        $page = $input->getPage();
        $offset = ($page - 1) * $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        return $this->gateway->findBy($query, $genres, $actors, $orderBy, $limit, $offset);
    }

    /**
     * @param WatchlistListInputInterface $input
     * @return int
     */
    private function countBy(WatchlistListInputInterface $input): int
    {
        $query = $input->getQuery();
        $genres = $input->getGenres();
        $actors = $input->getActors();

        return $this->gateway->countBy($query, $genres, $actors);
    }
}
