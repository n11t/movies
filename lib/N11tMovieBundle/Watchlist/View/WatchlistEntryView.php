<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\View;

use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class WatchlistEntryView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var MovieView
     */
    private $movie;

    /**
     * @var \DateTimeInterface
     */
    private $created;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return WatchlistEntryView
     */
    public function setId(int $id): WatchlistEntryView
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return MovieView
     */
    public function getMovie(): MovieView
    {
        return $this->movie;
    }

    /**
     * @param MovieView $movie
     * @return WatchlistEntryView
     */
    public function setMovie(MovieView $movie): WatchlistEntryView
    {
        $this->movie = $movie;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @param \DateTimeInterface $created
     * @return WatchlistEntryView
     */
    public function setCreated(\DateTimeInterface $created): WatchlistEntryView
    {
        $this->created = $created;
        return $this;
    }
}
