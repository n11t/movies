<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\View;

use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactory;

class WatchlistViewFactory implements WatchlistViewFactoryInterface
{

    /**
     * @var MovieViewFactory
     */
    private $movieViewFactory;

    public function __construct(MovieViewFactory $movieViewFactory)
    {
        $this->movieViewFactory = $movieViewFactory;
    }

    public function build(WatchlistEntry $entry): WatchlistEntryView
    {
        $view = new WatchlistEntryView();
        $view
            ->setId($entry->getId())
            ->setMovie($this->movieViewFactory->build($entry->getMovie()))
            ->setCreated($entry->getCreated())
        ;

        return $view;
    }
}
