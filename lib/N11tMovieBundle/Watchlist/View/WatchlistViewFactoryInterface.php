<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\View;

use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;

interface WatchlistViewFactoryInterface
{

    public function build(WatchlistEntry $entry): WatchlistEntryView;
}
