<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface WatchlistAddOutputInterface
{

    public function movieNotFound(int $id): void;

    public function movieIsInWatchlist(MovieView $view): void;

    public function movieAdded(MovieView $view): void;
}
