<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface WatchlistRemoveOutputInterface
{
    public function entryNotFoundForId(int $id): void;

    public function movieRemoved(MovieView $view): void;
}
