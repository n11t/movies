<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Watchlist\Output;

use N11t\Bundle\MovieBundle\Output\ListOutputInterface;
use N11t\Bundle\MovieBundle\Watchlist\View\WatchlistEntryView;

interface WatchlistListOutputInterface extends ListOutputInterface
{

    public function addEntry(WatchlistEntryView $view): void;
}
