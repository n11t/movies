<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Output;

interface ListOutputInterface
{

    public function setCount(int $count): void;
}
