<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Exception\MovieDelete;

use N11t\Bundle\MovieBundle\Exception\MovieDeleteException;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Throwable;

class InCollectionException extends MovieDeleteException
{

    /**
     * @var MovieView
     */
    private $view;

    public function __construct(MovieView $view)
    {
        $message = sprintf('Movie %d is still in the collection', $view->getId());

        parent::__construct($message, 403);
        $this->view = $view;
    }

    /**
     * @return MovieView
     */
    public function getView(): MovieView
    {
        return $this->view;
    }
}
