<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Exception\MovieDelete;

use N11t\Bundle\MovieBundle\Exception\MovieDeleteException;
use Throwable;

class MovieNotFoundException extends MovieDeleteException
{

    /**
     * @var int
     */
    private $id;

    public function __construct(int $id)
    {
        $message = sprintf('No movie found for id %d', $id);
        parent::__construct($message, 404);

        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
