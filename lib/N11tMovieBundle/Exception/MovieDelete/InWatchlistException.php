<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Exception\MovieDelete;

use N11t\Bundle\MovieBundle\Exception\MovieDeleteException;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class InWatchlistException extends MovieDeleteException
{
    /**
     * @var MovieView
     */
    private $view;

    public function __construct(MovieView $view)
    {
        $message = sprintf('Movie %d is still on the watchlist', $view->getId());

        parent::__construct($message, 403);
        $this->view = $view;
    }

    /**
     * @return MovieView
     */
    public function getView(): MovieView
    {
        return $this->view;
    }
}
