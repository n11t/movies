<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Exception\MovieCreate;

use N11t\Bundle\MovieBundle\Exception\MovieImportException;
use Throwable;

class MovieNotFoundException extends MovieImportException
{

    /**
     * @var string
     */
    private $imdbId;

    public function __construct(string $imdbId)
    {
        $message = sprintf('No movie found for imdbId: %s', $imdbId);
        parent::__construct($message, 404);

        $this->imdbId = $imdbId;
    }

    /**
     * @return string
     */
    public function getImdbId(): string
    {
        return $this->imdbId;
    }
}
