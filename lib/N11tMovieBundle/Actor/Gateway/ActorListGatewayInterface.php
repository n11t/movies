<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\Gateway;

use N11t\Bundle\MovieBundle\Entity\Actor;

interface ActorListGatewayInterface
{

    /**
     * @param string|null $query
     * @return int
     */
    public function countBy(?string $query): int;

    /**
     * @param string|null $query
     * @param array $orderBy
     * @param int|null $limit
     * @param int $offset
     *
     * @return Actor[]
     */
    public function findBy(?string $query, array $orderBy, ?int $limit, int $offset): array;
}
