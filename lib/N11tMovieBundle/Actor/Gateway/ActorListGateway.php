<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use N11t\Bundle\MovieBundle\Repository\ActorRepository;

class ActorListGateway implements ActorListGatewayInterface
{

    /**
     * @var ActorRepository
     */
    private $actorRepository;

    public function __construct(ActorRepository $actorRepository)
    {
        $this->actorRepository = $actorRepository;
    }

    /**
     * {@inheritDoc}
     * @throws NonUniqueResultException
     */
    public function countBy(?string $query): int
    {
        $qb = $this->prepareQueryBuilder($query)
            ->select('count(a.id)')
        ;

        $query = $qb->getQuery();

        $query->execute();

        return (int)$query->getSingleScalarResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(?string $query, array $orderBy, ?int $limit, int $offset): array
    {
        $qb = $this->prepareQueryBuilder($query);

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }
        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }
        foreach ($orderBy as $sort => $order) {
            $qb->orderBy("a.$sort", $order);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    private function prepareQueryBuilder(?string $query): QueryBuilder
    {
        $qb = $this->actorRepository->createQueryBuilder('a');

        if (is_string($query)) {
            $qb
                ->andWhere('a.name LIKE :query')
                ->setParameter('query', "%$query%")
            ;
        }

        return $qb;
    }
}
