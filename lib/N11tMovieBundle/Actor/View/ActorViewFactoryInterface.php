<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\View;

use N11t\Bundle\MovieBundle\Entity\Actor;

interface ActorViewFactoryInterface
{

    public function build(Actor $actor): ActorView;
}
