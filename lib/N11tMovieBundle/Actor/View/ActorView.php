<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\View;

class ActorView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ActorView
     */
    public function setId(int $id): ActorView
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ActorView
     */
    public function setName(string $name): ActorView
    {
        $this->name = $name;
        return $this;
    }
}
