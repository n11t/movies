<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\View;

use N11t\Bundle\MovieBundle\Entity\Actor;

class ActorViewFactory implements ActorViewFactoryInterface
{

    public function build(Actor $actor): ActorView
    {
        $view = new ActorView();
        $view
            ->setId($actor->getId())
            ->setName($actor->getName())
        ;

        return $view;
    }
}
