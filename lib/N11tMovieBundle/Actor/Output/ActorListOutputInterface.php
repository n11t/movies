<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\Output;

use N11t\Bundle\MovieBundle\Actor\View\ActorView;

interface ActorListOutputInterface
{

    public function setCount(int $count): void;

    public function addActor(ActorView $view): void;
}
