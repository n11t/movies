<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\Input;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;

interface ActorListInputInterface extends ListInputInterface
{

}
