<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Actor\UseCase;

use N11t\Bundle\MovieBundle\Actor\Gateway\ActorListGatewayInterface;
use N11t\Bundle\MovieBundle\Actor\Input\ActorListInputInterface;
use N11t\Bundle\MovieBundle\Actor\Output\ActorListOutputInterface;
use N11t\Bundle\MovieBundle\Actor\View\ActorViewFactoryInterface;
use N11t\Bundle\MovieBundle\Entity\Actor;

class ActorListUseCase
{

    /**
     * @var ActorListGatewayInterface
     */
    private $gateway;

    /**
     * @var ActorViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        ActorListGatewayInterface $gateway,
        ActorViewFactoryInterface $viewFactory
    ) {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(ActorListInputInterface $input, ActorListOutputInterface $output): void
    {
        $count = $this->countBy($input);
        $output->setCount($count);

        $actors = $this->findBy($input);
        foreach ($actors as $actor) {
            $view = $this->viewFactory->build($actor);

            $output->addActor($view);
        }
    }

    public function countBy(ActorListInputInterface $input): int
    {
        $query = $input->getQuery();

        return $this->gateway->countBy($query);
    }

    /**
     * @param ActorListInputInterface $input
     * @return Actor[]
     */
    private function findBy(ActorListInputInterface $input): array
    {
        $query = $input->getQuery();
        $orderBy = $input->getOrderBy();
        $limit = $input->getLimit();
        $page = $input->getPage();
        $offset = ($page - 1) * $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        return $this->gateway->findBy($query, $orderBy, $limit, $offset);
    }
}
