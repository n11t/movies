<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use N11t\Bundle\MovieBundle\Entity\Actor;

/**
 * Class ActorRepository
 * @package N11t\Bundle\MovieBundle\Repository
 *
 * @method Actor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Actor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Actor[] findAll()
 * @method Actor[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActorRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Actor::class);
    }

    public function persist(Actor $actor): void
    {
        $this->_em->persist($actor);
        $this->_em->flush();
    }
}
