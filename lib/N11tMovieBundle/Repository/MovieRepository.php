<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Repository;

use N11t\Bundle\MovieBundle\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class MovieRepository
 * @package App\Repository
 *
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Movie[] findAll()
 */
class MovieRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    /**
     * @param Movie $movie
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Movie $movie): void
    {
        $this->_em->persist($movie);
        $this->_em->flush($movie);
    }

    /**
     * @param Movie $movie
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Movie $movie): void
    {
        $this->_em->remove($movie);
        $this->_em->flush($movie);
    }

    public function countAll(): int
    {
        return $this->count([]);
    }
}
