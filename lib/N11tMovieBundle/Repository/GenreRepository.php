<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Repository;

use N11t\Bundle\MovieBundle\Entity\Genre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class GenreRepository
 * @package N11t\Bundle\MovieBundle\Repository
 *
 * @method Genre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Genre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Genre[] findAll()
 * @method Genre[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenreRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Genre::class);
    }

    public function persist(Genre $genre): void
    {
        $this->_em->persist($genre);
        $this->_em->flush();
    }
}
