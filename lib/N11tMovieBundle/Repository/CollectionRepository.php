<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\CollectionEntry;
use N11t\Bundle\MovieBundle\Entity\Movie;

/**
 * Class CollectionRepository
 * @package N11t\Bundle\MovieBundle\Repository
 *
 * @method CollectionEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method CollectionEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method CollectionEntry[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method CollectionEntry[] findAll()
 */
class CollectionRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CollectionEntry::class);
    }

    /**
     * @param CollectionEntry $collection
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(CollectionEntry $collection): void
    {
        $this->_em->persist($collection);
        $this->_em->flush($collection);
    }

    public function countAll(): int
    {
        return $this->count([]);
    }

    /**
     * @param CollectionEntry $entry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(CollectionEntry $entry): void
    {
        $this->_em->remove($entry);
        $this->_em->flush($entry);
    }

    /**
     * @param Movie $movie
     * @return bool
     */
    public function hasMovie(Movie $movie): bool
    {
        $qb = $this->createQueryBuilder('mc')
            ->select('count(mc.id)')
            ->andWhere('mc.movie = :movie')
            ->setParameter('movie', $movie);

        $query = $qb->getQuery();

        try {
            $count = (int)$query->getSingleScalarResult();
        } catch (NonUniqueResultException $exception) {
            throw new \RuntimeException('Query in hasMovie is broken', 500, $exception);
        }

        return $count > 0;
    }
}
