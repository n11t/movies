<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Entity\WatchlistEntry;

/**
 * Class WatchlistRepository
 * @package N11t\Bundle\MovieBundle\Repository
 *
 * @method WatchlistEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method WatchlistEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method WatchlistEntry[] findAll()
 * @method WatchlistEntry[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WatchlistRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WatchlistEntry::class);
    }

    /**
     * @param WatchlistEntry $entry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(WatchlistEntry $entry): void
    {
        $this->_em->persist($entry);
        $this->_em->flush($entry);
    }

    public function hasMovie(Movie $movie): bool
    {
        $qb = $this->createQueryBuilder('wl')
            ->select('count(wl.id)')
            ->andWhere('wl.movie = :movie')
            ->setParameter('movie', $movie);

        $query = $qb->getQuery();

        try {
            $count = (int)$query->getSingleScalarResult();
        } catch (NonUniqueResultException $exception) {
            throw new \RuntimeException('Query in hasMovie is broken', 500, $exception);
        }

        return $count > 0;
    }

    /**
     * @param WatchlistEntry $entry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(WatchlistEntry $entry): void
    {
        $this->_em->remove($entry);
        $this->_em->flush($entry);
    }
}
