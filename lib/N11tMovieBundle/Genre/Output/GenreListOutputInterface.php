<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\Output;

use N11t\Bundle\MovieBundle\Genre\View\GenreView;
use N11t\Bundle\MovieBundle\Output\ListOutputInterface;

interface GenreListOutputInterface extends ListOutputInterface
{

    public function addGenre(GenreView $view): void;
}
