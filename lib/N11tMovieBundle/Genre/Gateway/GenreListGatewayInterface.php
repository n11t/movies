<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\Gateway;

use N11t\Bundle\MovieBundle\Entity\Genre;

interface GenreListGatewayInterface
{

    /**
     * @param string $query
     * @param array $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return Genre[]
     */
    public function findBy(string $query, array $orderBy, ?int $limit, ?int $offset): array;

    /**
     * @param string $query
     * @return int
     */
    public function countBy(?string $query): int;
}
