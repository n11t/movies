<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\Gateway;

use N11t\Bundle\MovieBundle\Repository\GenreRepository;

class GenreListGateway implements GenreListGatewayInterface
{

    /**
     * @var GenreRepository
     */
    private $genreRepository;

    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(?string $query, array $orderBy, ?int $limit, ?int $offset): array
    {
        return $this->genreRepository->findAll();
    }

    /**
     * {@inheritDoc}
     */
    public function countBy(?string $query): int
    {
        return $this->genreRepository->count([]);
    }
}
