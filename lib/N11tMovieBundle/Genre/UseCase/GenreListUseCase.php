<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\UseCase;

use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Genre\Gateway\GenreListGatewayInterface;
use N11t\Bundle\MovieBundle\Genre\Input\GenreListInputInterface;
use N11t\Bundle\MovieBundle\Genre\Output\GenreListOutputInterface;
use N11t\Bundle\MovieBundle\Genre\View\GenreViewFactoryInterface;

class GenreListUseCase
{

    /**
     * @var GenreListGatewayInterface
     */
    private $gateway;

    /**
     * @var GenreViewFactoryInterface
     */
    private $genreViewFactory;

    public function __construct(
        GenreListGatewayInterface $gateway,
        GenreViewFactoryInterface $genreViewFactory
    ) {
        $this->gateway = $gateway;
        $this->genreViewFactory = $genreViewFactory;
    }

    public function process(GenreListInputInterface $input, GenreListOutputInterface $output): void
    {
        $maxCount = $this->countAll($input);
        $output->setCount($maxCount);

        $genres = $this->findAll($input);
        foreach ($genres as $genre) {
            $view = $this->genreViewFactory->build($genre);

            $output->addGenre($view);
        }
    }

    /**
     * @param GenreListInputInterface $input
     * @return Genre[]
     */
    private function findAll(GenreListInputInterface $input): array
    {
        $query = $input->getQuery();
        $orderBy = $input->getOrderBy();
        $limit = $input->getLimit();
        $page = $input->getPage();
        $offset = ($page - 1) * $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        return $this->gateway->findBy($query, $orderBy, $limit, $offset);
    }

    /**
     * @param GenreListInputInterface $input
     * @return int
     */
    private function countAll(GenreListInputInterface $input): int
    {
        $query = $input->getQuery();

        return $this->gateway->countBy($query);
    }
}
