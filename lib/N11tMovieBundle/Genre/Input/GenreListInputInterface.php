<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\Input;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;

interface GenreListInputInterface extends ListInputInterface
{
}
