<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\View;

class GenreView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GenreView
     */
    public function setId(int $id): GenreView
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return GenreView
     */
    public function setName(string $name): GenreView
    {
        $this->name = $name;
        return $this;
    }
}
