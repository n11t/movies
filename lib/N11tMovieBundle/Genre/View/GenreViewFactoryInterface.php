<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\View;

use N11t\Bundle\MovieBundle\Entity\Genre;

interface GenreViewFactoryInterface
{

    public function build(Genre $genre): GenreView;
}
