<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Genre\View;

use N11t\Bundle\MovieBundle\Entity\Genre;

class GenreViewFactory implements GenreViewFactoryInterface
{

    public function build(Genre $genre): GenreView
    {
        $view = new GenreView();
        $view
            ->setId($genre->getId())
            ->setName($genre->getName())
        ;

        return $view;
    }
}
