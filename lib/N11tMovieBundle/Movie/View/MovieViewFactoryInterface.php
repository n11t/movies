<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\View;

use N11t\Bundle\MovieBundle\Entity\Movie;

interface MovieViewFactoryInterface
{

    public function build(Movie $movie): MovieView;
}
