<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\View;

use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;
use N11t\Bundle\MovieBundle\Repository\WatchlistRepository;

class MovieViewFactory implements MovieViewFactoryInterface
{

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;

    /**
     * @var WatchlistRepository
     */
    private $watchlistRepository;

    public function __construct(
        CollectionRepository $collectionRepository,
        WatchlistRepository $watchlistRepository
    ) {
        $this->collectionRepository = $collectionRepository;
        $this->watchlistRepository = $watchlistRepository;
    }

    public function build(Movie $movie): MovieView
    {
        $view = new MovieView();
        $view
            ->setId($movie->getId())
            ->setImdbId($movie->getImdbId())
            ->setTitle($movie->getTitle())
            ->setLength($movie->getLength())
            ->setImageUrl($movie->getImageUrl())
            ->setDescription($movie->getDescription())
            ->setReleaseDate($movie->getReleaseDate())
            ->setInCollection($this->collectionRepository->hasMovie($movie))
            ->setInWatchlist($this->watchlistRepository->hasMovie($movie))
            ->setGenres($this->buildGenres($movie))
            ->setActores($this->buildActors($movie))
        ;

        return $view;
    }

    private function buildGenres(Movie $movie): array
    {
        $genres = [];
        foreach ($movie->getGenres() as $genre) {
            $genres[] = $genre->getName();
        }

        return $genres;
    }

    private function buildActors(Movie $movie): array
    {
        $actors = [];
        foreach ($movie->getActors() as $actor) {
            $actors[] = $actor->getName();
        }

        return $actors;
    }
}
