<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\View;

class MovieView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $imdbId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTimeInterface|null
     */
    private $releaseDate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $length;

    /**
     * @var string|null
     */
    private $imageUrl;

    /**
     * @var bool
     */
    private $inCollection;

    /**
     * @var bool
     */
    private $inWatchlist;

    /**
     * @var string[]
     */
    private $genres = [];

    /**
     * @var string[]
     */
    private $actores = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MovieView
     */
    public function setId(int $id): MovieView
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int|null $id
     * @return MovieView
     */
    /**
     * @return string
     */
    public function getImdbId(): string
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     * @return MovieView
     */
    public function setImdbId(string $imdbId): MovieView
    {
        $this->imdbId = $imdbId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return MovieView
     */
    public function setTitle(string $title): MovieView
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    /**
     * @param \DateTimeInterface|null $releaseDate
     * @return MovieView
     */
    public function setReleaseDate(?\DateTimeInterface $releaseDate): MovieView
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return MovieView
     */
    public function setDescription(string $description): MovieView
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     * @return MovieView
     */
    public function setLength(int $length): MovieView
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     * @return MovieView
     */
    public function setImageUrl(?string $imageUrl): MovieView
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInCollection(): bool
    {
        return $this->inCollection;
    }

    /**
     * @param bool $inCollection
     * @return MovieView
     */
    public function setInCollection(bool $inCollection): MovieView
    {
        $this->inCollection = $inCollection;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInWatchlist(): bool
    {
        return $this->inWatchlist;
    }

    /**
     * @param bool $inWatchlist
     * @return MovieView
     */
    public function setInWatchlist(bool $inWatchlist): MovieView
    {
        $this->inWatchlist = $inWatchlist;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getGenres(): array
    {
        return $this->genres;
    }

    /**
     * @param string[] $genres
     * @return MovieView
     */
    public function setGenres(array $genres): MovieView
    {
        $this->genres = $genres;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getActores(): array
    {
        return $this->actores;
    }

    /**
     * @param string[] $actores
     * @return MovieView
     */
    public function setActores(array $actores): MovieView
    {
        $this->actores = $actores;
        return $this;
    }
}
