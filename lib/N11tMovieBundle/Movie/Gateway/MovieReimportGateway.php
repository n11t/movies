<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Repository\ActorRepository;
use N11t\Bundle\MovieBundle\Repository\GenreRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;

class MovieReimportGateway implements MovieReimportGatewayInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var ActorRepository
     */
    private $actorRepository;

    /**
     * @var GenreRepository
     */
    private $genreRepository;

    public function __construct(
        MovieRepository $movieRepository,
        ActorRepository $actorRepository,
        GenreRepository $genreRepository
    ) {
        $this->movieRepository = $movieRepository;
        $this->actorRepository = $actorRepository;
        $this->genreRepository = $genreRepository;
    }

    /**
     * @param Movie $movie
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Movie $movie): void
    {
        $this->movieRepository->persist($movie);
    }

    public function persistActor(Actor $actor): void
    {
        $this->actorRepository->persist($actor);
    }

    public function persistGenre(Genre $genre): void
    {
        $this->genreRepository->persist($genre);
    }

    /**
     * @return Movie[]
     */
    public function findAll(): array
    {
        return $this->movieRepository->findAll();
    }

    /**
     * @param int $id
     * @return Movie|null
     */
    public function find(int $id): ?Movie
    {
        return $this->movieRepository->find($id);
    }

    public function findActorByName(string $name): ?Actor
    {
        return $this->actorRepository->findOneBy([
            'name' => $name,
        ]);
    }

    public function findGenreByName(string $name): ?Genre
    {
        return $this->genreRepository->findOneBy([
            'name' => $name,
        ]);
    }
}
