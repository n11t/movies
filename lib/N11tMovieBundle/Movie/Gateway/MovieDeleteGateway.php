<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Repository\WatchlistRepository;

class MovieDeleteGateway implements MovieDeleteGatewayInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;

    /**
     * @var WatchlistRepository
     */
    private $watchlistRepository;

    public function __construct(
        MovieRepository $movieRepository,
        CollectionRepository $collectionRepository,
        WatchlistRepository $watchlistRepository
    ) {
        $this->movieRepository = $movieRepository;
        $this->collectionRepository = $collectionRepository;
        $this->watchlistRepository = $watchlistRepository;
    }

    public function findMovieById(int $id): ?Movie
    {
        return $this->movieRepository->find($id);
    }

    /**
     * @param Movie $movie
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteMovie(Movie $movie): void
    {
        $this->movieRepository->remove($movie);
    }

    /**
     * @param Movie $movie
     * @return bool
     */
    public function isInCollection(Movie $movie): bool
    {
        return $this->collectionRepository->hasMovie($movie);
    }

    public function isInWatchlist(Movie $movie): bool
    {
        return $this->watchlistRepository->hasMovie($movie);
    }
}
