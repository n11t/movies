<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Repository\ActorRepository;
use N11t\Bundle\MovieBundle\Repository\GenreRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class MovieImportGateway implements MovieImportGatewayInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var GenreRepository
     */
    private $genreRepository;

    /**
     * @var ActorRepository
     */
    private $actorRepository;

    public function __construct(
        MovieRepository $movieRepository,
        GenreRepository $genreRepository,
        ActorRepository $actorRepository
    ) {
        $this->movieRepository = $movieRepository;
        $this->genreRepository = $genreRepository;
        $this->actorRepository = $actorRepository;
    }

    /**
     * @param Movie $movie
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persist(Movie $movie): void
    {
        $this->movieRepository->persist($movie);
    }

    public function persistActor(Actor $actor): void
    {
        $this->actorRepository->persist($actor);
    }

    public function persistGenre(Genre $genre): void
    {
        $this->genreRepository->persist($genre);
    }

    public function findByImdbId(string $imdbId): ?Movie
    {
        return $this->movieRepository->findOneBy([
            'imdbId' => $imdbId,
        ]);
    }

    public function findGenreByName(string $name): ?Genre
    {
        return $this->genreRepository->findOneBy([
            'name' => $name,
        ]);
    }

    public function findActorByName(string $name): ?Actor
    {
        return $this->actorRepository->findOneBy([
            'name' => $name,
        ]);
    }
}
