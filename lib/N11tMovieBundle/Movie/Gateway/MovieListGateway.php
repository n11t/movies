<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use Doctrine\ORM\QueryBuilder;

class MovieListGateway implements MovieListGatewayInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(
        ?string $query,
        ?array $genres,
        ?array $actors,
        array $orderBy = [],
        ?int $limit = null,
        ?int $offset = null
    ): array {
        $qb = $this->getQueryBuilder($query, $genres, $actors)
            ->groupBy('m.id')
        ;

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }
        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }
        foreach ($orderBy as $sort => $order) {
            $qb->addOrderBy('m.' . $sort, $order);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @return int
     * @throws NonUniqueResultException
     */
    public function countBy(?string $query, ?array $genres, ?array $actors): int
    {
        $qb = $this->getQueryBuilder($query, $genres, $actors);

        $qb->select('count(DISTINCT(m.id))');

        $query = $qb->getQuery();

        return (int)$query->getSingleScalarResult();
    }

    private function getQueryBuilder(?string $query, ?array $genres, ?array $actors): QueryBuilder
    {
        $qb = $this->movieRepository->createQueryBuilder('m');

        if (is_string($query)) {
            $qb
                ->orWhere($qb->expr()->like('m.imdbId', ':query'))
                ->orWhere($qb->expr()->like('m.title', ':query'))
                ->orWhere($qb->expr()->like('m.description', ':query'))
                ->setParameter('query', "%$query%")
            ;
        }
        if (is_array($genres) && count($genres) > 0) {
            $qb
                ->join('m.genres', 'mg')
                ->andWhere($qb->expr()->in('mg.id', $genres))
            ;
        }
        if (is_array($actors) && count($actors) > 0) {
                $qb
                    ->join('m.actors', 'ma')
                    ->andWhere($qb->expr()->in('ma.id', $actors))
                ;
        }

        return $qb;
    }
}
