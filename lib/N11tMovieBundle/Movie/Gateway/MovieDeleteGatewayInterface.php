<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use N11t\Bundle\MovieBundle\Entity\Movie;

interface MovieDeleteGatewayInterface
{

    public function findMovieById(int $id): ?Movie;

    public function deleteMovie(Movie $movie): void;

    public function isInCollection(Movie $movie): bool;

    public function isInWatchlist(Movie $movie): bool;
}
