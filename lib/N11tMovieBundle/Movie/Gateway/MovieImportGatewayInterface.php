<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;

interface MovieImportGatewayInterface
{

    public function persist(Movie $movie): void;

    public function persistActor(Actor $actor): void;

    public function persistGenre(Genre $genre): void;

    public function findByImdbId(string $imdbId): ?Movie;

    public function findGenreByName(string $name): ?Genre;

    public function findActorByName(string $name): ?Actor;
}
