<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;

interface MovieReimportGatewayInterface
{

    public function persist(Movie $movie): void;

    public function persistActor(Actor $actor): void;

    public function persistGenre(Genre $genre): void;

    /**
     * @return Movie[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Movie|null
     */
    public function find(int $id): ?Movie;

    public function findActorByName(string $name): ?Actor;

    public function findGenreByName(string $name): ?Genre;
}
