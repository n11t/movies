<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Gateway;

use N11t\Bundle\MovieBundle\Entity\Movie;

interface MovieListGatewayInterface
{

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @param array $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return Movie[]
     */
    public function findBy(
        ?string $query,
        ?array $genres,
        ?array $actors,
        array $orderBy = [],
        ?int $limit = null,
        ?int $offset = null
    ): array;

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @return int
     */
    public function countBy(?string $query, ?array $genres, ?array $actors): int;
}
