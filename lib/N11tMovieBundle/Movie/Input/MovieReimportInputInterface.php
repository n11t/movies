<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Input;

interface MovieReimportInputInterface
{

    /**
     * The movie ids to update as array or null.
     * When null will be returned all movies will be updated.
     *
     * @return int[]|null
     */
    public function getMovieIds(): ?array;
}
