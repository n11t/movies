<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Input;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;

interface MovieListInputInterface extends ListInputInterface
{

    /**
     * @return int[]|null
     */
    public function getGenres(): ?array;

    /**
     * @return int[]|null
     */
    public function getActors(): ?array;
}
