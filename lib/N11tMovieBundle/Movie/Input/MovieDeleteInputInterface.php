<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Input;

interface MovieDeleteInputInterface
{

    public function getId(): int;
}
