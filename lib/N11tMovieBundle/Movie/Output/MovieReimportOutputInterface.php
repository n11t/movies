<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface MovieReimportOutputInterface
{

    public function movieNotFound(string $imdbId): void;

    public function reimported(MovieView $view): void;

    public function movieForIdNotFound(int $id): void;
}
