<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface MovieDeleteOutputInterface
{

    public function setMovie(MovieView $view): void;
}
