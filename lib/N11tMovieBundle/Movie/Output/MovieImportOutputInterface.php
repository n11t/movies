<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface MovieImportOutputInterface
{

    public function setMovie(MovieView $movie): void;
}
