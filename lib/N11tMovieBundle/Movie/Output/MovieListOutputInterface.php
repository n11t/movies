<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Output\ListOutputInterface;

interface MovieListOutputInterface extends ListOutputInterface
{

    public function addMovie(MovieView $view): void;
}
