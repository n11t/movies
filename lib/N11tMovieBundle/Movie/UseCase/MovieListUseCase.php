<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\UseCase;

use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Movie\Gateway\MovieListGatewayInterface;
use N11t\Bundle\MovieBundle\Movie\Input\MovieListInputInterface;
use N11t\Bundle\MovieBundle\Movie\Output\MovieListOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;

class MovieListUseCase
{

    /**
     * @var MovieListGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieViewFactoryInterface
     */
    private $movieViewFactory;

    public function __construct(MovieListGatewayInterface $gateway, MovieViewFactoryInterface $movieViewFactory)
    {
        $this->gateway = $gateway;
        $this->movieViewFactory = $movieViewFactory;
    }

    public function process(MovieListInputInterface $input, MovieListOutputInterface $output): void
    {
        $count = $this->countBy($input);
        $output->setCount($count);

        $movies = $this->findBy($input);
        foreach ($movies as $movie) {
            $view = $this->movieViewFactory->build($movie);

            $output->addMovie($view);
        }
    }

    /**
     * @param MovieListInputInterface $input
     * @return Movie[]
     */
    private function findBy(MovieListInputInterface $input): array
    {
        $query = $input->getQuery();
        $orderBy = $input->getOrderBy();
        $genres = $input->getGenres();
        $actors = $input->getActors();
        $limit = $input->getLimit();
        $page = $input->getPage();
        $offset = ($page - 1) * $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        return $this->gateway->findBy($query, $genres, $actors, $orderBy, $limit, $offset);
    }

    private function countBy(MovieListInputInterface $input)
    {
        $query = $input->getQuery();
        $genres = $input->getGenres();
        $actors = $input->getActors();

        return $this->gateway->countBy($query, $genres, $actors);
    }
}
