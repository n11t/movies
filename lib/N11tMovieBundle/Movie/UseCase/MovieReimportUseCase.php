<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\UseCase;

use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;
use N11t\Bundle\MovieAdapterBundle\Exception\MovieNotFoundException;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Movie\Gateway\MovieReimportGatewayInterface;
use N11t\Bundle\MovieBundle\Movie\Input\MovieReimportInputInterface;
use N11t\Bundle\MovieBundle\Movie\Output\MovieReimportOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;

class MovieReimportUseCase
{
    use MovieImportTrait;

    /**
     * @var MovieReimportGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieApiAdapterInterface
     */
    private $apiAdapter;

    /**
     * @var MovieViewFactoryInterface
     */
    private $movieViewFactory;

    public function __construct(
        MovieReimportGatewayInterface $gateway,
        MovieApiAdapterInterface $apiAdapter,
        MovieViewFactoryInterface $movieViewFactory
    ) {
        $this->gateway = $gateway;
        $this->apiAdapter = $apiAdapter;
        $this->movieViewFactory = $movieViewFactory;
    }

    public function process(MovieReimportInputInterface $input, MovieReimportOutputInterface $output): void
    {
        $ids = $input->getMovieIds();

        if (is_array($ids)) {
            $this->reimportMoviesByIds($output, ...$ids);
        } else {
            $this->reimportAllMovies($output);
        }
    }

    private function reimportMoviesByIds(MovieReimportOutputInterface $output, int ...$ids): void
    {
        foreach ($ids as $id) {
            $movie = $this->gateway->find($id);
            if (!$movie instanceof Movie) {
                $output->movieForIdNotFound($id);
                continue;
            }

            $this->reimportMovie($output, $movie);
        }
    }

    private function reimportAllMovies(MovieReimportOutputInterface $output): void
    {
        $movies = $this->gateway->findAll();

        foreach ($movies as $movie) {
            $this->reimportMovie($output, $movie);
        }
    }

    private function reimportMovie(MovieReimportOutputInterface $output, Movie $movie): void
    {
        $apiMovie = $this->findApiMovie($movie);
        if (!$apiMovie instanceof MovieInterface) {
            $output->movieNotFound($movie->getImdbId());
            return;
        }

        $this->updateMovie($movie, $apiMovie);

        $this->gateway->persist($movie);

        $view = $this->movieViewFactory->build($movie);

        $output->reimported($view);
    }

    private function findApiMovie(Movie $movie): ?MovieInterface
    {
        try {
            return $this->apiAdapter->findByImdbId($movie->getImdbId());
        } catch (MovieNotFoundException $exception) {
            return null;
        }
    }

    protected function findActorByName(string $name): ?Actor
    {
        return $this->gateway->findActorByName($name);
    }

    protected function findGenreByName(string $name): ?Genre
    {
        return $this->gateway->findGenreByName($name);
    }

    protected function persistActor(Actor $actor): void
    {
        $this->gateway->persistActor($actor);
    }

    protected function persistGenre(Genre $genre): void
    {
        $this->gateway->persistGenre($genre);
    }
}
