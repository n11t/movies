<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\UseCase;

use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\DuplicateMovieException;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieImportException;
use N11t\Bundle\MovieBundle\Movie\Gateway\MovieImportGatewayInterface;
use N11t\Bundle\MovieBundle\Movie\Input\MovieImportInputInterface;
use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;
use N11t\Bundle\MovieAdapterBundle\Exception\MovieNotFoundException as AdapterMovieNotFoundException;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieBundle\Movie\Output\MovieImportOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;

class MovieImportUseCase
{
    use MovieImportTrait;

    /**
     * @var MovieImportGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieApiAdapterInterface
     */
    private $apiAdapter;

    /**
     * @var MovieViewFactoryInterface
     */
    private $movieViewFactory;

    public function __construct(
        MovieImportGatewayInterface $gateway,
        MovieApiAdapterInterface $apiAdapter,
        MovieViewFactoryInterface $movieViewFactory
    ) {
        $this->gateway = $gateway;
        $this->apiAdapter = $apiAdapter;
        $this->movieViewFactory = $movieViewFactory;
    }

    /**
     * @param MovieImportInputInterface $input
     * @param MovieImportOutputInterface $output
     * @throws MovieImportException
     */
    public function process(MovieImportInputInterface $input, MovieImportOutputInterface $output): void
    {
        $this->checkIfMovieExists($input->getImdbId());

        $dbMovie = $this->findMovie($input);

        $movie = new Movie();
        $this->updateMovie($movie, $dbMovie);

        $this->persistMovie($movie);

        $view = $this->movieViewFactory->build($movie);

        $output->setMovie($view);
    }

    /**
     * @param MovieImportInputInterface $input
     * @return MovieInterface
     * @throws MovieNotFoundException
     */
    private function findMovie(MovieImportInputInterface $input): MovieInterface
    {
        $imdbId = $input->getImdbId();
        try {
            return $this->apiAdapter->findByImdbId($imdbId);
        } catch (AdapterMovieNotFoundException $exception) {
            throw new MovieNotFoundException($imdbId);
        }
    }

    private function persistMovie(Movie $movie): void
    {
        $this->gateway->persist($movie);
    }

    /**
     * @param string $imdbId
     * @throws DuplicateMovieException
     */
    private function checkIfMovieExists(string $imdbId): void
    {
        $movie = $this->gateway->findByImdbId($imdbId);

        if ($movie instanceof Movie) {
            throw new DuplicateMovieException($imdbId);
        }
    }

    protected function findActorByName(string $name): ?Actor
    {
        return $this->gateway->findActorByName($name);
    }

    protected function findGenreByName(string $name): ?Genre
    {
        return $this->gateway->findGenreByName($name);
    }

    protected function persistActor(Actor $actor): void
    {
        $this->gateway->persistActor($actor);
    }

    protected function persistGenre(Genre $genre): void
    {
        $this->gateway->persistGenre($genre);
    }
}
