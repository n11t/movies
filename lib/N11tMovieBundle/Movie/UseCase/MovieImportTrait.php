<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\UseCase;

use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;

trait MovieImportTrait
{

    protected function updateMovie(Movie $movie, MovieInterface $dbMovie): void
    {
        $genres = $this->buildGenres(...$dbMovie->getGenres());
        $actors = $this->buildActors(...$dbMovie->getActors());

        $movie
            ->setImdbId($dbMovie->getImdbId())
            ->setTitle($dbMovie->getTitle())
            ->setReleaseDate($dbMovie->getReleaseDate())
            ->setDescription($dbMovie->getDescription())
            ->setLength($dbMovie->getLength())
            ->setImageUrl($dbMovie->getImageUrl())
            ->setActors($actors)
            ->setGenres($genres)
        ;
    }

    private function buildGenres(string ...$names): array
    {
        $result = [];
        foreach($names as $name) {
            $name = trim($name);
            $genre = $this->findGenreByName($name);
            if (!$genre instanceof Genre) {
                $genre = new Genre();
                $genre
                    ->setName($name)
                ;
                $this->persistGenre($genre);
            }

            $result[] = $genre;
        }

        return $result;
    }

    private function buildActors(string ...$names): array
    {
        $result = [];
        foreach($names as $name) {
            $name = trim($name);
            $actor = $this->findActorByName($name);
            if (!$actor instanceof Actor) {
                $actor = new Actor();
                $actor
                    ->setName($name)
                ;
                $this->persistActor($actor);
            }

            $result[] = $actor;
        }

        return $result;
    }

    abstract protected function findActorByName(string $name): ?Actor;

    abstract protected function findGenreByName(string $name): ?Genre;

    abstract protected function persistActor(Actor $actor): void;

    abstract protected function persistGenre(Genre $genre): void;
}
