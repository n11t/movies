<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Movie\UseCase;

use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\InCollectionException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\InWatchlistException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieDeleteException;
use N11t\Bundle\MovieBundle\Movie\Gateway\MovieDeleteGatewayInterface;
use N11t\Bundle\MovieBundle\Movie\Input\MovieDeleteInputInterface;
use N11t\Bundle\MovieBundle\Movie\Output\MovieDeleteOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;

class MovieDeleteUseCase
{

    /**
     * @var MovieDeleteGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieViewFactoryInterface
     */
    private $movieViewFactory;

    public function __construct(
        MovieDeleteGatewayInterface $gateway,
        MovieViewFactoryInterface $movieViewFactory
    ) {
        $this->gateway = $gateway;
        $this->movieViewFactory = $movieViewFactory;
    }

    /**
     * @param MovieDeleteInputInterface $input
     * @param MovieDeleteOutputInterface $output
     * @throws MovieDeleteException
     */
    public function process(MovieDeleteInputInterface $input, MovieDeleteOutputInterface $output): void
    {
        $movie = $this->findMovie($input);

        $view = $this->movieViewFactory->build($movie);

        $this->checkDependencies($movie);

        $this->deleteMovie($movie);

        $output->setMovie($view);
    }

    /**
     * @param MovieDeleteInputInterface $input
     * @return Movie
     * @throws MovieNotFoundException
     */
    private function findMovie(MovieDeleteInputInterface $input): Movie
    {
        $movie = $this->gateway->findMovieById($input->getId());

        if (!$movie instanceof Movie) {
            throw new MovieNotFoundException($input->getId());
        }

        return $movie;
    }

    private function deleteMovie(Movie $movie): void
    {
        $this->gateway->deleteMovie($movie);
    }

    /**
     * @param Movie $movie
     * @throws MovieDeleteException
     */
    private function checkDependencies(Movie $movie): void
    {
        if ($this->gateway->isInCollection($movie)) {
            $view = $this->movieViewFactory->build($movie);

            throw new InCollectionException($view);
        }

        if ($this->gateway->isInWatchlist($movie)) {
            $view = $this->movieViewFactory->build($movie);

            throw new InWatchlistException($view);
        }
    }
}
