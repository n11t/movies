<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Search\Input;

interface SearchInputInterface
{

    public function getQuery(): string;
}
