<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Search\Output;

use N11t\Bundle\MovieBundle\Search\View\SearchResultView;

interface SearchOutputInterface
{

    /**
     * @param SearchResultView $view
     */
    public function addResult(SearchResultView $view): void;
}
