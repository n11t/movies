<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Search\View;

class SearchResultView
{

    /**
     * @var string
     */
    private $imdbId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTimeInterface|null
     */
    private $releaseDate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $length;

    /**
     * @var string|null
     */
    private $imageUrl;

    /**
     * @var bool
     */
    private $imported;

    /**
     * @var bool
     */
    private $inWatchlist;

    /**
     * @var bool
     */
    private $inCollection;

    /**
     * @return string
     */
    public function getImdbId(): string
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     * @return SearchResultView
     */
    public function setImdbId(string $imdbId): SearchResultView
    {
        $this->imdbId = $imdbId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return SearchResultView
     */
    public function setTitle(string $title): SearchResultView
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    /**
     * @param \DateTimeInterface|null $releaseDate
     * @return SearchResultView
     */
    public function setReleaseDate(?\DateTimeInterface $releaseDate): SearchResultView
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return SearchResultView
     */
    public function setDescription(string $description): SearchResultView
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     * @return SearchResultView
     */
    public function setLength(int $length): SearchResultView
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     * @return SearchResultView
     */
    public function setImageUrl(?string $imageUrl): SearchResultView
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImported(): bool
    {
        return $this->imported;
    }

    /**
     * @param bool $imported
     * @return SearchResultView
     */
    public function setImported(bool $imported): SearchResultView
    {
        $this->imported = $imported;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInWatchlist(): bool
    {
        return $this->inWatchlist;
    }

    /**
     * @param bool $inWatchlist
     * @return SearchResultView
     */
    public function setInWatchlist(bool $inWatchlist): SearchResultView
    {
        $this->inWatchlist = $inWatchlist;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInCollection(): bool
    {
        return $this->inCollection;
    }

    /**
     * @param bool $inCollection
     * @return SearchResultView
     */
    public function setInCollection(bool $inCollection): SearchResultView
    {
        $this->inCollection = $inCollection;
        return $this;
    }
}
