<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Search\View;

use Doctrine\ORM\NonUniqueResultException;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use N11t\Bundle\MovieBundle\Repository\WatchlistRepository;

class SearchResultViewFactory implements SearchResultViewFactoryInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var WatchlistRepository
     */
    private $watchlistRepository;

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;

    public function __construct(
        MovieRepository $movieRepository,
        WatchlistRepository $watchlistRepository,
        CollectionRepository $collectionRepository
    ) {
        $this->movieRepository = $movieRepository;
        $this->watchlistRepository = $watchlistRepository;
        $this->collectionRepository = $collectionRepository;
    }

    public function build(MovieInterface $movie): SearchResultView
    {
        $imported = $this->isMovieImported($movie);
        $inWatchlist = $this->isMovieInWatchlist($movie);
        $inCollection = $this->isMovieInCollection($movie);

        $view = new SearchResultView();
        $view
            ->setImdbId($movie->getImdbId())
            ->setTitle($movie->getTitle())
            ->setLength($movie->getLength())
            ->setImageUrl($movie->getImageUrl())
            ->setDescription($movie->getDescription())
            ->setReleaseDate($movie->getReleaseDate())
            ->setImported($imported)
            ->setInWatchlist($inWatchlist)
            ->setInCollection($inCollection)
        ;

        return $view;
    }

    private function isMovieImported(MovieInterface $movie): bool
    {
        $result = $this->movieRepository->findOneBy([
            'imdbId' => $movie->getImdbId(),
        ]);

        return $result instanceof Movie;
    }

    private function isMovieInWatchlist(MovieInterface $movie): bool
    {
        $qb = $this->watchlistRepository->createQueryBuilder('wl')
            ->select('count(wl.id)')
            ->join('wl.movie', 'm')
            ->andWhere('m.imdbId = :imdb_id')
            ->setParameter('imdb_id', $movie->getImdbId());

        $query = $qb->getQuery();

        try {
            $count = (int)$query->getSingleScalarResult();
        } catch (NonUniqueResultException $exception) {
            throw new \RuntimeException('Query in hasMovie is broken', 500, $exception);
        }

        return $count > 0;
    }

    private function isMovieInCollection(MovieInterface $movie): bool
    {
        $qb = $this->collectionRepository->createQueryBuilder('cl')
            ->select('count(cl.id)')
            ->join('cl.movie', 'm')
            ->andWhere('m.imdbId = :imdb_id')
            ->setParameter('imdb_id', $movie->getImdbId());

        $query = $qb->getQuery();

        try {
            $count = (int)$query->getSingleScalarResult();
        } catch (NonUniqueResultException $exception) {
            throw new \RuntimeException('Query in hasMovie is broken', 500, $exception);
        }

        return $count > 0;
    }
}
