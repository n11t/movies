<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Search\View;

use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;

interface SearchResultViewFactoryInterface
{

    public function build(MovieInterface $movie): SearchResultView;
}
