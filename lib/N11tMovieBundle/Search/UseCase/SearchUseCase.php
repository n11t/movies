<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Search\UseCase;

use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;
use N11t\Bundle\MovieBundle\Search\Input\SearchInputInterface;
use N11t\Bundle\MovieBundle\Search\Output\SearchOutputInterface;
use N11t\Bundle\MovieBundle\Search\View\SearchResultViewFactoryInterface;

class SearchUseCase
{

    /**
     * @var MovieApiAdapterInterface
     */
    private $apiAdapter;

    /**
     * @var SearchResultViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        MovieApiAdapterInterface $apiAdapter,
        SearchResultViewFactoryInterface $viewFactory
    ) {
        $this->apiAdapter = $apiAdapter;
        $this->viewFactory = $viewFactory;
    }

    public function process(SearchInputInterface $input, SearchOutputInterface $output): void
    {
        $apiMovies = $this->findApiMovies($input);

        foreach ($apiMovies as $movie) {
            $view = $this->viewFactory->build($movie);

            $output->addResult($view);
        }
    }

    /**
     * @param SearchInputInterface $input
     * @return MovieInterface[]
     */
    private function findApiMovies(SearchInputInterface $input): array
    {
        return $this->apiAdapter->findBy($input->getQuery());
    }
}
