<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\View;

use N11t\Bundle\MovieBundle\Entity\CollectionEntry;

interface CollectionViewFactoryInterface
{

    public function build(CollectionEntry $entry): CollectionEntryView;
}
