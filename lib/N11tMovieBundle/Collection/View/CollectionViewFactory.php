<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\View;

use N11t\Bundle\MovieBundle\Entity\CollectionEntry;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactory;

class CollectionViewFactory implements CollectionViewFactoryInterface
{

    /**
     * @var MovieViewFactory
     */
    private $movieViewFactory;

    public function __construct(MovieViewFactory $movieViewFactory)
    {
        $this->movieViewFactory = $movieViewFactory;
    }

    public function build(CollectionEntry $entry): CollectionEntryView
    {
        $movieView = $this->movieViewFactory->build($entry->getMovie());

        return new CollectionEntryView(
            $entry->getId(),
            $movieView,
            $entry->getCreated()
        );
    }

}
