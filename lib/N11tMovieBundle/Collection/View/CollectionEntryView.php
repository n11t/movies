<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\View;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class CollectionEntryView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var MovieView
     */
    private $movie;

    /**
     * @var \DateTimeInterface
     */
    private $created;

    /**
     * CollectionEntryView constructor.
     * @param int $id
     * @param MovieView $movie
     * @param \DateTimeInterface $created
     */
    public function __construct(int $id, MovieView $movie, \DateTimeInterface $created)
    {
        $this->id = $id;
        $this->movie = $movie;
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return MovieView
     */
    public function getMovie(): MovieView
    {
        return $this->movie;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
}
