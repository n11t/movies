<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface CollectionAddOutputInterface
{

    public function movieNotFound(int $id): void;

    public function movieAdded(MovieView $view): void;

    public function movieIsInCollection(MovieView $view): void;
}
