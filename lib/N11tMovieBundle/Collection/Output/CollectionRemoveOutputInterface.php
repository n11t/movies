<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Output;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface CollectionRemoveOutputInterface
{

    public function entryNotFoundForId(int $id): void;

    public function movieRemoved(MovieView $view): void;
}
