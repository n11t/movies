<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Output;

use N11t\Bundle\MovieBundle\Collection\View\CollectionEntryView;
use N11t\Bundle\MovieBundle\Output\ListOutputInterface;

interface CollectionListOutputInterface extends ListOutputInterface
{

    public function addEntry(CollectionEntryView $view): void;
}
