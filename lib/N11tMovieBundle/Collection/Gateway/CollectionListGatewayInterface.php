<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Gateway;

use N11t\Bundle\MovieBundle\Entity\CollectionEntry;

interface CollectionListGatewayInterface
{

    /**
     * @param string|null $query
     * @param array $orderBy
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @param int|null $limit
     * @param int $offset
     * @return CollectionEntry[]
     */
    public function findBy(?string $query, array $orderBy, ?array $genres, ?array $actors, ?int $limit, ?int $offset): array;

    /**
     * @param string|null $query
     * @param int[]|null $genres
     * @param int[]|null $actors
     * @return int
     */
    public function countBy(?string $query, ?array $genres, ?array $actors): int;
}
