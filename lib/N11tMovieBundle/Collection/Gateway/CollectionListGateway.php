<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use N11t\Bundle\MovieBundle\Collection\Gateway\CollectionListGatewayInterface;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;

class CollectionListGateway implements CollectionListGatewayInterface
{

    /**
     * @var CollectionRepository
     */
    private $movieCollectionRepository;

    public function __construct(CollectionRepository $movieCollectionRepository)
    {
        $this->movieCollectionRepository = $movieCollectionRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(?string $query, array $orderBy, ?array $genres, ?array $actors, ?int $limit, ?int $offset): array
    {
        $qb = $this->getQueryBuilder($query, $genres, $actors)
            ->groupBy('mc.movie')
        ;

        if (is_int($limit)) {
            $qb->setMaxResults($limit);
        }
        if (is_int($offset)) {
            $qb->setFirstResult($offset);
        }
        foreach ($orderBy as $sort => $order) {
            $qb->addOrderBy('m.' . $sort, $order);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * {@inheritDoc}
     * @throws NonUniqueResultException
     */
    public function countBy(?string $query, ?array $genres, ?array $actors): int
    {
        $qb = $this->getQueryBuilder($query, $genres, $actors)
            ->select('count(DISTINCT(m.id))')
        ;

        $query = $qb->getQuery();

        return (int)$query->getSingleScalarResult();
    }

    private function getQueryBuilder(?string $query, ?array $genres, ?array $actors): QueryBuilder
    {
        $qb = $this->movieCollectionRepository->createQueryBuilder('mc')
            ->join('mc.movie', 'm')
        ;

        if (is_string($query)) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('m.imdbId', ':query'),
                    $qb->expr()->like('m.title', ':query'),
                    $qb->expr()->like('m.description', ':query')
                )
            );
            $qb->setParameter('query', "%$query%");
        }
        if (is_array($genres) && count($genres) > 0) {
            $qb
                ->join('m.genres', 'mg')
                ->andWhere($qb->expr()->in('mg.id', $genres))
            ;
        }
        if (is_array($actors) && count($actors) > 0) {
            $qb
                ->join('m.actors', 'ma')
                ->andWhere($qb->expr()->in('ma.id', $actors))
            ;
        }

        return $qb;
    }
}
