<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\CollectionEntry;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;

class CollectionRemoveGateway implements CollectionRemoveGatewayInterface
{

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;

    public function __construct(CollectionRepository $collectionRepository)
    {
        $this->collectionRepository = $collectionRepository;
    }

    /**
     * @param int $id
     * @return CollectionEntry|null
     */
    public function find(int $id): ?CollectionEntry
    {
        return $this->collectionRepository->find($id);
    }

    /**
     * @param CollectionEntry $entry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(CollectionEntry $entry)
    {
        $this->collectionRepository->remove($entry);
    }
}
