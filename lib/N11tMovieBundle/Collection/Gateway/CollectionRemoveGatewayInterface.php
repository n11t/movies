<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Gateway;

use N11t\Bundle\MovieBundle\Entity\CollectionEntry;

interface CollectionRemoveGatewayInterface
{

    public function find(int $id): ?CollectionEntry;

    public function remove(CollectionEntry $entry);
}
