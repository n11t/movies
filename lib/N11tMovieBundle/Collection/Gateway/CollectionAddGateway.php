<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use N11t\Bundle\MovieBundle\Entity\CollectionEntry;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;

class CollectionAddGateway implements CollectionAddGatewayInterface
{

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;

    public function __construct(
        MovieRepository $movieRepository,
        CollectionRepository $collectionRepository
    ) {
        $this->movieRepository = $movieRepository;
        $this->collectionRepository = $collectionRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function find(int $id): ?Movie
    {
        return $this->movieRepository->find($id);
    }

    /**
     * {@inheritDoc}
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addToCollection(Movie $movie): void
    {
        $collection = new CollectionEntry($movie);

        $this->collectionRepository->persist($collection);
    }

    /**
     * @param Movie $movie
     * @return bool
     * @throws NonUniqueResultException
     */
    public function isInCollection(Movie $movie): bool
    {
        return $this->collectionRepository->hasMovie($movie);
    }
}
