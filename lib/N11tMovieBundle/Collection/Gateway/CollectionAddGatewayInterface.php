<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Gateway;

use N11t\Bundle\MovieBundle\Entity\Movie;

interface CollectionAddGatewayInterface
{

    /**
     * @param int $id
     * @return Movie|null
     */
    public function find(int $id): ?Movie;

    /**
     * @param Movie $movie
     */
    public function addToCollection(Movie $movie): void;

    /**
     * @param Movie $movie
     * @return bool
     */
    public function isInCollection(Movie $movie): bool;
}
