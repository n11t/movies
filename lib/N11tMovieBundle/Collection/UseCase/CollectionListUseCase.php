<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\UseCase;

use N11t\Bundle\MovieBundle\Collection\View\CollectionViewFactory;
use N11t\Bundle\MovieBundle\Entity\CollectionEntry;
use N11t\Bundle\MovieBundle\Collection\Gateway\CollectionListGatewayInterface;
use N11t\Bundle\MovieBundle\Collection\Input\CollectionListInputInterface;
use N11t\Bundle\MovieBundle\Collection\Output\CollectionListOutputInterface;

class CollectionListUseCase
{

    /**
     * @var CollectionListGatewayInterface
     */
    private $gateway;

    /**
     * @var CollectionViewFactory
     */
    private $viewFactory;

    public function __construct(
        CollectionListGatewayInterface $gateway,
        CollectionViewFactory $viewFactory)
    {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(CollectionListInputInterface $input, CollectionListOutputInterface $output): void
    {
        $count = $this->countBy($input);
        $output->setCount($count);

        $collectionEntry = $this->find($input);
        foreach ($collectionEntry as $entry) {
            $view = $this->viewFactory->build($entry);

            $output->addEntry($view);
        }
    }

    /**
     * @param CollectionListInputInterface $input
     * @return CollectionEntry[]
     */
    private function find(CollectionListInputInterface $input): array
    {
        $query = $input->getQuery();
        $genres = $input->getGenres();
        $actors = $input->getActors();
        $orderBy = $input->getOrderBy();
        $limit = $input->getLimit();
        $page = $input->getPage();
        $offset = ($page - 1) * $limit;
        if ($offset < 0) {
            $offset = 0;
        }

        return $this->gateway->findBy($query, $orderBy, $genres, $actors, $limit, $offset);
    }

    /**
     * @param CollectionListInputInterface $input
     * @return int
     */
    private function countBy(CollectionListInputInterface $input): int
    {
        $query = $input->getQuery();
        $genres = $input->getGenres();
        $actors = $input->getActors();

        return $this->gateway->countBy($query, $genres, $actors);
    }
}
