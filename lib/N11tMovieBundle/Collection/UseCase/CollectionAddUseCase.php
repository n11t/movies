<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\UseCase;

use N11t\Bundle\MovieBundle\Collection\Gateway\CollectionAddGatewayInterface;
use N11t\Bundle\MovieBundle\Collection\Input\CollectionAddInputInterface;
use N11t\Bundle\MovieBundle\Collection\Output\CollectionAddOutputInterface;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;

class CollectionAddUseCase
{

    /**
     * @var CollectionAddGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        CollectionAddGatewayInterface $gateway,
        MovieViewFactoryInterface $viewFactory
    ) {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(CollectionAddInputInterface $input, CollectionAddOutputInterface $output): void
    {
        $ids = $input->getIds();

        foreach ($ids as $id) {
            $movie = $this->findMovie((int)$id);
            if (!$movie instanceof Movie) {
                $output->movieNotFound($id);

                continue;
            }

            $view = $this->viewFactory->build($movie);
            if ($this->isMovieInCollection($movie)) {
                $output->movieIsInCollection($view);

                continue;
            }

            $this->addMovie($movie);

            $output->movieAdded($view);
        }
    }

    private function findMovie(int $id): ?Movie
    {
        return $this->gateway->find($id);
    }

    private function addMovie(Movie $movie): void
    {
        $this->gateway->addToCollection($movie);
    }

    private function isMovieInCollection(Movie $movie): bool
    {
        return $this->gateway->isInCollection($movie);
    }
}
