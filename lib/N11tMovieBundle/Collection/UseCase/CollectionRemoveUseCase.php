<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\UseCase;

use N11t\Bundle\MovieBundle\Collection\Gateway\CollectionRemoveGatewayInterface;
use N11t\Bundle\MovieBundle\Collection\Input\CollectionRemoveInputInterface;
use N11t\Bundle\MovieBundle\Collection\Output\CollectionRemoveOutputInterface;
use N11t\Bundle\MovieBundle\Entity\CollectionEntry;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactoryInterface;

class CollectionRemoveUseCase
{

    /**
     * @var CollectionRemoveGatewayInterface
     */
    private $gateway;

    /**
     * @var MovieViewFactoryInterface
     */
    private $viewFactory;

    public function __construct(
        CollectionRemoveGatewayInterface $gateway,
        MovieViewFactoryInterface $viewFactory
    ) {
        $this->gateway = $gateway;
        $this->viewFactory = $viewFactory;
    }

    public function process(CollectionRemoveInputInterface $input, CollectionRemoveOutputInterface $output): void
    {
        $ids = $input->getIds();

        foreach ($ids as $id) {
            $entry = $this->findEntry((int)$id);
            if (!$entry instanceof CollectionEntry) {
                $output->entryNotFoundForId($id);

                continue;
            }

            $movie = $entry->getMovie();

            $this->removeEntry($entry);

            $view = $this->viewFactory->build($movie);

            $output->movieRemoved($view);
        }
    }

    private function findEntry(int $id): ?CollectionEntry
    {
        return $this->gateway->find($id);
    }

    private function removeEntry(CollectionEntry $entry): void
    {
        $this->gateway->remove($entry);
    }
}
