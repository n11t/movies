<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Input;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;

interface CollectionListInputInterface extends ListInputInterface
{

    /**
     * @return int[]|null
     */
    public function getGenres(): ?array;

    /**
     * @return int[]|null
     */
    public function getActors(): ?array;
}
