<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Collection\Input;

interface CollectionRemoveInputInterface
{

    /**
     * @return int[]
     */
    public function getIds(): array;
}
