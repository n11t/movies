<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('n11t_movie');
        $root = $builder->getRootNode();
        $root
            ->children()
                ->arrayNode('gateway')
                    ->children()
                        ->arrayNode('movie')
                            ->children()
                                ->scalarNode('Movie')
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $builder;
    }
}
