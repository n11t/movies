<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use N11t\Bundle\MovieBundle\Entity\Movie;

/**
 * Class Genre
 * @package App\UrlGenerator
 *
 * @ORM\Entity()
 * @ORM\Table(name="genre")
 */
class Genre
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var Movie[]
     * @ORM\ManyToMany(targetEntity="N11t\Bundle\MovieBundle\Entity\Movie", mappedBy="genres")
     */
    private $movies = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Genre
     */
    public function setId(int $id): Genre
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Genre
     */
    public function setName(string $name): Genre
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Movie[]
     */
    public function getMovies(): array
    {
        return $this->movies;
    }

    /**
     * @param Movie[] $movies
     * @return Genre
     */
    public function setMovies(array $movies): Genre
    {
        $this->movies = $movies;
        return $this;
    }
}
