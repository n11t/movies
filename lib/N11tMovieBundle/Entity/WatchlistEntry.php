<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class WatchlistEntry
 * @package N11t\Bundle\MovieBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="watch_list_entry")
 */
class WatchlistEntry
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Movie
     * @ORM\OneToOne(targetEntity="N11t\Bundle\MovieBundle\Entity\Movie")
     * @ORM\JoinColumn(name="movie_id", referencedColumnName="id", nullable=false)
     */
    private $movie;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="created", type="datetimetz", nullable=false)
     */
    private $created;

    public function __construct(Movie $movie)
    {
        $this->movie = $movie;
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Movie
     */
    public function getMovie(): Movie
    {
        return $this->movie;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
}
