<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class MovieCollection
 * @package N11t\Bundle\MovieBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="collection_entry", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="idx_col_entry_movie", columns={"movie_id"})
 * })
 */
class CollectionEntry
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Movie
     * @ORM\OneToOne(targetEntity="N11t\Bundle\MovieBundle\Entity\Movie")
     * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     */
    private $movie;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="created", type="datetimetz")
     */
    private $created;

    public function __construct(Movie $movie)
    {
        $this->created = new \DateTime();
        $this->movie = $movie;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Movie
     */
    public function getMovie(): Movie
    {
        return $this->movie;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }
}
