<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Actor
 * @package N11t\Bundle\MovieBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="actor")
 */
class Actor
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var Movie[]
     * @ORM\ManyToMany(targetEntity="N11t\Bundle\MovieBundle\Entity\Movie", mappedBy="actors")
     */
    private $movies = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Actor
     */
    public function setId(int $id): Actor
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Actor
     */
    public function setName(string $name): Actor
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Movie[]
     */
    public function getMovies(): array
    {
        return $this->movies;
    }

    /**
     * @param Movie[] $movies
     * @return Actor
     */
    public function setMovies(array $movies): Actor
    {
        $this->movies = $movies;
        return $this;
    }
}
