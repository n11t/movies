<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="movie")
 */
class Movie
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="imdb_id", type="string", nullable=false)
     */
    private $imdbId;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    private $title;

    /**
     * @var \DateTimeInterface|null
     * @ORM\Column(name="release_date", type="date", nullable=true)
     */
    private $releaseDate;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var int
     * @ORM\Column(name="length", type="integer", nullable=false)
     */
    private $length;

    /**
     * @var string
     * @ORM\Column(name="image_url", type="text", nullable=false)
     */
    private $imageUrl;

    /**
     * @var Genre[]
     * @ORM\ManyToMany(targetEntity="N11t\Bundle\MovieBundle\Entity\Genre", inversedBy="movies", cascade={"persist"})
     * @ORM\JoinTable(name="movie_genres")
     */
    private $genres = [];

    /**
     * @var Actor[]
     * @ORM\ManyToMany(targetEntity="N11t\Bundle\MovieBundle\Entity\Actor", inversedBy="movies", cascade={"persist"})
     * @ORM\JoinTable(name="movie_actors")
     */
    private $actors = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImdbId(): string
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     * @return Movie
     */
    public function setImdbId(string $imdbId): Movie
    {
        $this->imdbId = $imdbId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Movie
     */
    public function setTitle(string $title): Movie
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    /**
     * @param \DateTimeInterface|null $releaseDate
     * @return Movie
     */
    public function setReleaseDate(?\DateTimeInterface $releaseDate): Movie
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Movie
     */
    public function setDescription(string $description): Movie
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     * @return Movie
     */
    public function setLength(int $length): Movie
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     * @return Movie
     */
    public function setImageUrl(string $imageUrl): Movie
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return Genre[]
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @param Genre[] $genres
     * @return Movie
     */
    public function setGenres(array $genres): Movie
    {
        $this->genres = $genres;
        return $this;
    }

    /**
     * @return Actor[]
     */
    public function getActors()
    {
        return $this->actors;
    }

    /**
     * @param Actor[] $actors
     * @return Movie
     */
    public function setActors(array $actors): Movie
    {
        $this->actors = $actors;
        return $this;
    }
}
