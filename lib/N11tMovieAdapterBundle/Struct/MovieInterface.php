<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\Struct;

interface MovieInterface
{

    public function getImdbId(): string;

    public function getTitle(): string;

    public function getDescription(): string;

    public function getLength(): int;

    public function getReleaseDate(): ?\DateTimeInterface;

    public function getImageUrl(): ?string;

    /**
     * @return string[]
     */
    public function getGenres(): array;

    /**
     * @return string[]
     */
    public function getActors(): array;
}
