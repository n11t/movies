<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle;

use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;

interface MovieApiAdapterRegistryInterface
{

    public function getMovieAdapter(): MovieApiAdapterInterface;
}
