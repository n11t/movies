<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('n11t_movie_adapter');
        $root = $treeBuilder->getRootNode();
        $root
            ->children()
                ->scalarNode('adapter_name')
                    ->isRequired()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
