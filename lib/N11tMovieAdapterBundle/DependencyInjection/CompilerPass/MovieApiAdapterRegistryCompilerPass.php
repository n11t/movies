<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\DependencyInjection\CompilerPass;

use N11t\Bundle\MovieAdapterBundle\MovieApiAdapterRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class MovieApiAdapterRegistryCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has('n11t_bundle_movie_adapter.movie_api_adapter_registry')) {
            return;
        }

        $definition = $container->findDefinition('n11t_bundle_movie_adapter.movie_api_adapter_registry');

        $taggedServices = $container->findTaggedServiceIds('n11t_bundle_movie_adapter.movie_api_adapter');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addAdapter', [new Reference($id)]);
        }
    }
}
