<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\Exception;

use Exception;

class MovieApiAdapterException extends Exception
{

}
