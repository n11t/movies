<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\Exception;

use Throwable;

class MovieNotFoundException extends MovieApiAdapterException
{

    /**
     * @var string
     */
    private $imdbId;

    public function __construct(string $imdbId)
    {
        $message = sprintf('No movie found for imdbId ' . $imdbId);
        parent::__construct($message, 404);

        $this->imdbId = $imdbId;
    }

    /**
     * @return string
     */
    public function getImdbId(): string
    {
        return $this->imdbId;
    }
}
