<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\Exception;

use Throwable;

class MovieAdapterNotFoundException extends \RuntimeException
{

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $message = sprintf('No movie adapter found for given name %s', $name);

        parent::__construct($message, 404);

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
