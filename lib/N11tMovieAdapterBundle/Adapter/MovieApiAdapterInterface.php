<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle\Adapter;

use N11t\Bundle\MovieAdapterBundle\Exception\MovieNotFoundException;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;

interface MovieApiAdapterInterface
{

    /**
     * A unique name for this Adapter.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $imdbId
     * @return MovieInterface
     * @throws MovieNotFoundException
     */
    public function findByImdbId(string $imdbId): MovieInterface;

    /**
     * @param string $query
     * @return MovieInterface[]
     */
    public function findBy(string $query): array;
}
