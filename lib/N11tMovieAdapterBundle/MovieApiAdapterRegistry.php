<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle;

use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;
use N11t\Bundle\MovieAdapterBundle\Exception\MovieAdapterNotFoundException;

class MovieApiAdapterRegistry implements MovieApiAdapterRegistryInterface
{

    /**
     * @var array<string, MovieApiAdapterInterface>
     */
    private $adapter = [];

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addAdapter(MovieApiAdapterInterface $movieApiAdapter): void
    {
        $this->adapter[$movieApiAdapter->getName()] = $movieApiAdapter;
    }

    public function getMovieAdapter(): MovieApiAdapterInterface
    {
        $adapter = $this->adapter[$this->name] ?? null;
        if (!$adapter instanceof MovieApiAdapterInterface) {
            throw new MovieAdapterNotFoundException($this->name);
        }

        return $adapter;
    }
}
