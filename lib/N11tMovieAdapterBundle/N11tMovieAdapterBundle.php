<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieAdapterBundle;

use N11t\Bundle\MovieAdapterBundle\DependencyInjection\CompilerPass\MovieApiAdapterRegistryCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class N11tMovieAdapterBundle extends Bundle
{

        public function build(ContainerBuilder $container)
        {
            parent::build($container);

            $container->addCompilerPass(new MovieApiAdapterRegistryCompilerPass());
        }
}
