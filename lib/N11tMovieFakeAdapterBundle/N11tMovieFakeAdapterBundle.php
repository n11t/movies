<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieFakeAdapterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class N11tMovieFakeAdapterBundle extends Bundle
{
}
