<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieFakeAdapterBundle;

use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;

class Movies
{

    private const movies = [
        [
            Movie::KEY_IMDB_ID => 'tt0111161',
            Movie::KEY_TITLE => 'The Shawshank Redemption',
            Movie::KEY_RELEASE_DATE => '14 October 1994',
            Movie::KEY_DESCRIPTION => 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
            Movie::KEY_LENGTH => 142,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0068646',
            Movie::KEY_TITLE => 'The Godfather',
            Movie::KEY_RELEASE_DATE => '24 March 1972',
            Movie::KEY_DESCRIPTION => 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
            Movie::KEY_LENGTH => 175,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0071562',
            Movie::KEY_TITLE => 'The Godfather: Part II',
            Movie::KEY_RELEASE_DATE => '20 December 1974',
            Movie::KEY_DESCRIPTION => 'The early life and career of Vito Corleone in 1920s New York City is portrayed, while his son, Michael, expands and tightens his grip on the family crime syndicate.',
            Movie::KEY_LENGTH => 202,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,701,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0468569',
            Movie::KEY_TITLE => 'The Dark Knight',
            Movie::KEY_RELEASE_DATE => '18 July 2008',
            Movie::KEY_DESCRIPTION => 'When the menace known as The Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham. The Dark Knight must accept one of the greatest psychological and physical tests of his ability to fight injustice.',
            Movie::KEY_LENGTH => 152,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0050083',
            Movie::KEY_TITLE => '12 Angry Men',
            Movie::KEY_RELEASE_DATE => '10 April 1957',
            Movie::KEY_DESCRIPTION => 'A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.',
            Movie::KEY_LENGTH => 96,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY1000_CR0,0,649,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0108052',
            Movie::KEY_TITLE => 'Schindler\'s List',
            Movie::KEY_RELEASE_DATE => ' 4 February 1994',
            Movie::KEY_DESCRIPTION => 'In German-occupied Poland during World War II, industrialist Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.',
            Movie::KEY_LENGTH => 195,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,666,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0167260',
            Movie::KEY_TITLE => 'The Lord of the Rings: The Return of the King',
            Movie::KEY_RELEASE_DATE => '17 December 2003',
            Movie::KEY_DESCRIPTION => 'Gandalf and Aragorn lead the World of Men against Sauron\'s army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.',
            Movie::KEY_LENGTH => 201,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,675,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0110912',
            Movie::KEY_TITLE => 'Pulp Fiction',
            Movie::KEY_RELEASE_DATE => '14 October 1994',
            Movie::KEY_DESCRIPTION => 'The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',
            Movie::KEY_LENGTH => 144,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,686,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0060196',
            Movie::KEY_TITLE => 'The Good, the Bad and the Ugly',
            Movie::KEY_RELEASE_DATE => '29 December 1967',
            Movie::KEY_DESCRIPTION => 'A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.',
            Movie::KEY_LENGTH => 178,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BOTQ5NDI3MTI4MF5BMl5BanBnXkFtZTgwNDQ4ODE5MDE@._V1_SY1000_CR0,0,656,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0137523',
            Movie::KEY_TITLE => 'Fight Club',
            Movie::KEY_RELEASE_DATE => '15 October 1999',
            Movie::KEY_DESCRIPTION => 'An insomniac office worker and a devil-may-care soapmaker form an underground fight club that evolves into something much, much more.',
            Movie::KEY_LENGTH => 139,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BMmEzNTkxYjQtZTc0MC00YTVjLTg5ZTEtZWMwOWVlYzY0NWIwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,666,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0120737',
            Movie::KEY_TITLE => 'The Lord of the Rings: The Fellowship of the Ring',
            Movie::KEY_RELEASE_DATE => '19 December 2001',
            Movie::KEY_DESCRIPTION => 'A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.',
            Movie::KEY_LENGTH => 178,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0109830',
            Movie::KEY_TITLE => 'Forrest Gump',
            Movie::KEY_RELEASE_DATE => '6 July 1994',
            Movie::KEY_DESCRIPTION => 'The presidencies of Kennedy and Johnson, the events of Vietnam, Watergate, and other history unfold through the perspective of an Alabama man with an IQ of 75.',
            Movie::KEY_LENGTH => 142,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt1375666',
            Movie::KEY_TITLE => 'Inception',
            Movie::KEY_RELEASE_DATE => '16 July 2010',
            Movie::KEY_DESCRIPTION => 'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.',
            Movie::KEY_LENGTH => 148,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0080684',
            Movie::KEY_TITLE => 'Star Wars: Episode V - The Empire Strikes Back',
            Movie::KEY_RELEASE_DATE => '20 June 1980',
            Movie::KEY_DESCRIPTION => 'After the Rebels are brutally overpowered by the Empire on the ice planet Hoth, Luke Skywalker begins Jedi training with Yoda, while his friends are pursued by Darth Vader.',
            Movie::KEY_LENGTH => 124,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,641,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0167261',
            Movie::KEY_TITLE => 'The Lord of the Rings: The Two Towers',
            Movie::KEY_RELEASE_DATE => '18 December 2002',
            Movie::KEY_DESCRIPTION => '',
            Movie::KEY_LENGTH => 179,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BNGE5MzIyNTAtNWFlMC00NDA2LWJiMjItMjc4Yjg1OWM5NzhhXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,684,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0133093',
            Movie::KEY_TITLE => 'The Matrix',
            Movie::KEY_RELEASE_DATE => '31 March 1999',
            Movie::KEY_DESCRIPTION => 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.',
            Movie::KEY_LENGTH => 136,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,665,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0073486',
            Movie::KEY_TITLE => 'One Flew Over the Cuckoo\'s Nest',
            Movie::KEY_RELEASE_DATE => '21 November 1975',
            Movie::KEY_DESCRIPTION => 'A criminal pleads insanity after getting into trouble again and once in the mental institution rebels against the oppressive nurse and rallies up the scared patients.',
            Movie::KEY_LENGTH => 133,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY1000_CR0,0,672,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0099685',
            Movie::KEY_TITLE => 'Goodfellas',
            Movie::KEY_RELEASE_DATE => '21 September 1990',
            Movie::KEY_DESCRIPTION => 'The story of Henry Hill and his life in the mob, covering his relationship with his wife Karen Hill and his mob partners Jimmy Conway and Tommy DeVito in the Italian-American crime syndicate.',
            Movie::KEY_LENGTH => 146,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX667_CR0,0,667,999_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0047478',
            Movie::KEY_TITLE => 'Seven Samurai',
            Movie::KEY_RELEASE_DATE => '19 November 1956',
            Movie::KEY_DESCRIPTION => 'A poor village under attack by bandits recruits seven unemployed samurai to help them defend themselves.',
            Movie::KEY_LENGTH => 207,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BOWE4ZDdhNmMtNzE5ZC00NzExLTlhNGMtY2ZhYjYzODEzODA1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY1000_CR0,0,709,1000_AL_.jpg',
        ],
        [
            Movie::KEY_IMDB_ID => 'tt0114369',
            Movie::KEY_TITLE => 'Se7en',
            Movie::KEY_RELEASE_DATE => '22 September 1995',
            Movie::KEY_DESCRIPTION => 'Two detectives, a rookie and a veteran, hunt a serial killer who uses the seven deadly sins as his motives.',
            Movie::KEY_LENGTH => 127,
            Movie::KEY_IMAGE_URL => 'https://m.media-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,639,1000_AL_.jpg',
        ],
    ];

    public static function findByImdbId(string $imdbId): ?MovieInterface
    {
        foreach (self::movies as $movie) {
            if ($movie[Movie::KEY_IMDB_ID] === $imdbId) {
                return new Movie($movie);
            }
        }

        return null;
    }

    /**
     * @param string $query
     * @return MovieInterface[]
     */
    public static function findBy(string $query): array
    {
        $query = strtolower($query);

        $results = [];
        foreach (self::movies as $movie) {
            $haystack = $movie[Movie::KEY_IMDB_ID] . $movie[Movie::KEY_TITLE] . $movie[Movie::KEY_DESCRIPTION];
            $haystack = strtolower($haystack);

            if (strpos($haystack, $query) !== false) {
                $results[] = new Movie($movie);
            }
        }

        return $results;
    }
}
