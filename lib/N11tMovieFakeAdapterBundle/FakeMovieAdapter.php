<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieFakeAdapterBundle;

use N11t\Bundle\MovieAdapterBundle\Adapter\MovieApiAdapterInterface;
use N11t\Bundle\MovieAdapterBundle\Exception\MovieNotFoundException;
use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;

class FakeMovieAdapter implements MovieApiAdapterInterface
{

    public function getName(): string
    {
        return 'fake';
    }

    /**
     * {@inheritDoc}
     */
    public function findByImdbId(string $imdbId): MovieInterface
    {
        $movie = Movies::findByImdbId($imdbId);
        if (!$movie instanceof MovieInterface) {
            throw new MovieNotFoundException($imdbId);
        }

        return $movie;
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(string $query): array
    {
        return Movies::findBy($query);
    }
}
