<?php
declare(strict_types=1);

namespace N11t\Bundle\MovieFakeAdapterBundle;

use N11t\Bundle\MovieAdapterBundle\Struct\MovieInterface;

class Movie implements MovieInterface
{

    public const KEY_IMDB_ID = 'imdb_id';

    public const KEY_TITLE = 'title';

    public const KEY_DESCRIPTION = 'description';

    public const KEY_LENGTH = 'length';

    public const KEY_RELEASE_DATE = 'release_date';

    public const KEY_IMAGE_URL = 'image_url';

    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getImdbId(): string
    {
        return $this->data[self::KEY_IMDB_ID];
    }

    public function getTitle(): string
    {
        return $this->data[self::KEY_TITLE];
    }

    public function getDescription(): string
    {
        return $this->data[self::KEY_DESCRIPTION];
    }

    public function getLength(): int
    {
        return $this->data[self::KEY_LENGTH];
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        $dateString = $this->data[self::KEY_RELEASE_DATE];
        if (is_string($dateString)) {
            return new \DateTime($dateString);
        }

        return null;
    }

    public function getImageUrl(): ?string
    {
        return $this->data[self::KEY_IMAGE_URL];
    }

    public function getGenres(): array
    {
        return [];
    }

    public function getActors(): array
    {
        return [];
    }
}
