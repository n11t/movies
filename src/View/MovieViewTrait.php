<?php
declare(strict_types=1);

namespace App\View;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

trait MovieViewTrait
{

    protected function formatMovieName(MovieView $view): string
    {
        $year = 'N/A';
        if ($view->getReleaseDate() instanceof \DateTimeInterface) {
            $year = $view->getReleaseDate()->format('Y');
        }

        return sprintf('%s (%s)', $view->getTitle(), $year);
    }
}
