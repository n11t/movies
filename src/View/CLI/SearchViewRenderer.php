<?php
declare(strict_types=1);

namespace App\View\CLI;

use App\Formatter\Movie\MovieFormatter;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Search\View\SearchResultView;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class SearchViewRenderer
{

    /**
     * @var MovieFormatter
     */
    private $movieFormatter;

    public function __construct(MovieFormatter $movieFormatter)
    {
        $this->movieFormatter = $movieFormatter;
    }

    /**
     * @param OutputInterface $output
     * @param SearchResultView ...$views
     */
    public function renderList(OutputInterface $output, SearchResultView ...$views): void
    {
        $table = new Table($output);
        $table->setHeaders([
            'Imdb ID',
            'Title',
            'Year',
            'Length',
            'Imported'
        ]);
        foreach ($views as $resultView) {
            $releaseDate = 'N/A';
            if ($resultView->getReleaseDate() instanceof \DateTimeInterface) {
                $releaseDate = $resultView->getReleaseDate()->format('Y-m-d');
            }

            $table->addRow([
                $resultView->getImdbId(),
                $resultView->getTitle(),
                $releaseDate,
                $this->movieFormatter->formatMovieLength($resultView->getLength()),
                $resultView->isImported() ? 'yes' : 'no'
            ]);
        }
        $table->render();
    }
}
