<?php
declare(strict_types=1);

namespace App\View\CLI;

use App\Formatter\Movie\MovieFormatter;
use N11t\Bundle\MovieBundle\Watchlist\View\WatchlistEntryView;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class WatchlistViewRenderer
{

    /**
     * @var MovieFormatter
     */
    private $movieFormatter;

    public function __construct(MovieFormatter $movieFormatter)
    {
        $this->movieFormatter = $movieFormatter;
    }

    public function renderList(OutputInterface $output, WatchlistEntryView ...$views): void
    {
        $table = new Table($output);
        $table->setHeaders([
            'Id',
            'Title',
            'Year',
            'Length',
            'Added',
        ]);
        foreach ($views as $entryView) {
            $movie = $entryView->getMovie();

            $table->addRow([
                $entryView->getId(),
                $movie->getTitle(),
                $movie->getReleaseDate()->format('Y'),
                $this->movieFormatter->formatMovieLength($movie->getLength()),
                $entryView->getCreated()->format('Y-m-d'),
            ]);
        }
        $table->render();
    }
}
