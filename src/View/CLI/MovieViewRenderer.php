<?php
declare(strict_types=1);

namespace App\View\CLI;

use App\Formatter\Movie\MovieFormatter;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class MovieViewRenderer
{

    /**
     * @var MovieFormatter
     */
    private $movieFormatter;

    public function __construct(MovieFormatter $movieFormatter)
    {
        $this->movieFormatter = $movieFormatter;
    }

    /**
     * @param OutputInterface $output
     * @param MovieView ...$views
     */
    public function renderList(OutputInterface $output, MovieView ...$views): void
    {
        $table = new Table($output);
        $table->setHeaders([
            'Id',
            'Imdb ID',
            'Title',
            'Year',
            'Length',
            'Collection',
        ]);
        foreach ($views as $movie) {
            $table->addRow([
                $movie->getId(),
                $movie->getImdbId(),
                $movie->getTitle(),
                $movie->getReleaseDate()->format('Y'),
                $this->movieFormatter->formatMovieLength($movie->getLength()),
                $movie->isInCollection() ? 'yes' : 'no'
            ]);
        }
        $table->render();
    }
}
