<?php
declare(strict_types=1);

namespace App\View\CLI;

use App\Formatter\Movie\MovieFormatter;
use N11t\Bundle\MovieBundle\Collection\View\CollectionEntryView;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class CollectionViewRenderer
{

    /**
     * @var MovieFormatter
     */
    private $movieFormatter;

    public function __construct(MovieFormatter $movieFormatter)
    {
        $this->movieFormatter = $movieFormatter;
    }

    /**
     * @param OutputInterface $output
     * @param CollectionEntryView ...$views
     */
    public function renderList(OutputInterface $output, CollectionEntryView ...$views): void
    {
        $table = new Table($output);
        $table->setHeaders([
            'Id',
            'Title',
            'Year',
            'Length',
            'Added',
        ]);
        foreach ($views as $entryView) {
            $movie = $entryView->getMovie();

            $table->addRow([
                $entryView->getId(),
                $movie->getTitle(),
                $movie->getReleaseDate()->format('Y'),
                $this->movieFormatter->formatMovieLength($movie->getLength()),
                $entryView->getCreated()->format('Y-m-d'),
            ]);
        }
        $table->render();
    }
}
