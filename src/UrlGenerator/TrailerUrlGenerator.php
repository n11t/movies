<?php
declare(strict_types=1);

namespace App\UrlGenerator;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class TrailerUrlGenerator
{

    public function generate(MovieView $view): string
    {
        $releaseDate = '';
        if ($view->getReleaseDate() instanceof \DateTimeInterface) {
            $releaseDate = $view->getReleaseDate()->format('Y');
        }

        $queries = [
            'trailer',
            $view->getTitle(),
            $releaseDate,
        ];

        $url = sprintf('https://www.youtube.com/results?search_query=%s', implode(' ', $queries));

        return $url;
    }
}
