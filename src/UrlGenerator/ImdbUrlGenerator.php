<?php
declare(strict_types=1);

namespace App\UrlGenerator;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class ImdbUrlGenerator
{

    public function generate(MovieView $view): string
    {
        return sprintf('https://www.imdb.com/title/%s/', $view->getImdbId());
    }
}
