<?php
declare(strict_types=1);

namespace App\Formatter\Movie;

class MovieFormatter
{

    public function formatMovieLength(int $minutes): string
    {
        $hours = (int)($minutes / 60);
        $minutes -= ($hours * 60);

        if ($hours < 1) {
            return sprintf('%dmin', $minutes);
        }

        return sprintf('%dh %dmin', $hours, $minutes);
    }
}
