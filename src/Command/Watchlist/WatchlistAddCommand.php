<?php
declare(strict_types=1);

namespace App\Command\Watchlist;

use App\Input\CLI\Watchlist\CLIWatchlistAddInput;
use App\Output\CLI\Watchlist\CLIWatchlistAddOutput;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistAddUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WatchlistAddCommand extends Command
{

    /**
     * @var WatchlistAddUseCase
     */
    private $useCase;

    public function __construct(WatchlistAddUseCase $useCase)
    {
        parent::__construct();

        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('watchlist:add')
            ->setDescription('Add movie to watchlist')
            ->addArgument('movie_ids', InputArgument::IS_ARRAY, 'The movie ids to add')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIWatchlistAddInput($input);
        $cliOutput = new CLIWatchlistAddOutput($output);

        $this->useCase->process($cliInput, $cliOutput);
    }
}
