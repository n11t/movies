<?php
declare(strict_types=1);

namespace App\Command\Watchlist;

use App\Input\CLI\Watchlist\CLIWatchlistListInput;
use App\Output\CLI\Watchlist\CLIWatchlistListOutput;
use App\View\CLI\WatchlistViewRenderer;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistListUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class WatchlistListCommand extends Command
{

    /**
     * @var WatchlistListUseCase
     */
    private $useCase;

    /**
     * @var WatchlistViewRenderer
     */
    private $viewRenderer;

    public function __construct(
        WatchlistListUseCase $useCase,
        WatchlistViewRenderer $viewRenderer
    ) {
        parent::__construct();

        $this->useCase = $useCase;
        $this->viewRenderer = $viewRenderer;
    }

    protected function configure()
    {
        $this
            ->setName('watchlist:list')
            ->setDescription('List all movies in the collection')
            ->addArgument('query', InputArgument::OPTIONAL, 'The query to search for', null)
            ->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'The limit', 10)
            ->addOption('page', 'p', InputOption::VALUE_REQUIRED, 'The page', 1)
            ->addOption('sort', 's', InputOption::VALUE_REQUIRED, 'Order by property', 'id')
            ->addOption('order', 'o', InputOption::VALUE_REQUIRED, 'Sort direction', 'ASC')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIWatchlistListInput($input);
        $cliOutput = new CLIWatchlistListOutput();

        $this->useCase->process($cliInput, $cliOutput);

        $this->viewRenderer->renderList($output, ...$cliOutput->entries);
    }
}
