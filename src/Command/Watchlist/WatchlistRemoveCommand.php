<?php
declare(strict_types=1);

namespace App\Command\Watchlist;

use App\Input\CLI\Watchlist\CLIWatchlistRemoveInput;
use App\Output\CLI\Watchlist\CLIWatchlistAddOutput;
use App\Output\CLI\Watchlist\CLIWatchlistRemoveOutput;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistRemoveUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WatchlistRemoveCommand extends Command
{
    /**
     * @var WatchlistRemoveUseCase
     */
    private $useCase;

    public function __construct(WatchlistRemoveUseCase $useCase)
    {
        parent::__construct();

        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('watchlist:remove')
            ->addArgument('watchlist_ids', InputArgument::IS_ARRAY, 'The watchlist ids to remove')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIWatchlistRemoveInput($input);
        $cliOutput = new CLIWatchlistRemoveOutput($output);

        $this->useCase->process($cliInput, $cliOutput);
    }
}
