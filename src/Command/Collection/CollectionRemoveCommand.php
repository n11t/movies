<?php
declare(strict_types=1);

namespace App\Command\Collection;

use App\Input\CLI\Collection\CLICollectionRemoveInput;
use App\Output\CLI\Collection\CLICollectionRemoveOutput;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionRemoveUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CollectionRemoveCommand extends Command
{

    /**
     * @var CollectionRemoveUseCase
     */
    private $useCase;

    public function __construct(CollectionRemoveUseCase $useCase)
    {
        parent::__construct();
        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('collection:remove')
            ->addArgument('collection_ids', InputArgument::IS_ARRAY, 'The collection ids to remove.', [])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLICollectionRemoveInput($input);
        $cliOutput = new CLICollectionRemoveOutput($output);

        $this->useCase->process($cliInput, $cliOutput);
    }
}
