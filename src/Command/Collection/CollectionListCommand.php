<?php
declare(strict_types=1);

namespace App\Command\Collection;

use App\Input\CLI\Collection\CLICollectionListInput;
use App\Output\CLI\Collection\CLICollectionListOutput;
use App\View\CLI\CollectionViewRenderer;
use App\View\CLI\MovieViewRenderer;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionListUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CollectionListCommand extends Command
{

    /**
     * @var CollectionListUseCase
     */
    private $useCase;

    /**
     * @var CollectionViewRenderer
     */
    private $viewRenderer;

    public function __construct(
        CollectionListUseCase $useCase,
        CollectionViewRenderer $viewRenderer
    ) {
        parent::__construct();

        $this->useCase = $useCase;
        $this->viewRenderer = $viewRenderer;
    }

    protected function configure()
    {
        $this
            ->setName('collection:list')
            ->setDescription('List all movies in the collection')
            ->addArgument('query', InputArgument::OPTIONAL, 'The query to search for', null)
            ->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'The limit', 10)
            ->addOption('page', 'p', InputOption::VALUE_REQUIRED, 'The page', 1)
            ->addOption('sort', 's', InputOption::VALUE_REQUIRED, 'Order by property', 'id')
            ->addOption('order', 'o', InputOption::VALUE_REQUIRED, 'Sort direction', 'ASC')
            ->addOption('genres', 'g', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'The genres to search for', null)
            ->addOption('actors', 'a', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'The Authors to search for', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLICollectionListInput($input);
        $cliOutput = new CLICollectionListOutput();

        $this->useCase->process($cliInput, $cliOutput);

        $this->viewRenderer->renderList($output, ...$cliOutput->movies);
    }
}
