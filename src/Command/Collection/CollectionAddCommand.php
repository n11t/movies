<?php
declare(strict_types=1);

namespace App\Command\Collection;

use App\Input\CLI\Collection\CLICollectionAddInput;
use App\Output\CLI\Collection\CLICollectionAddOutput;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionAddUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CollectionAddCommand extends Command
{

    /**
     * @var CollectionAddUseCase
     */
    private $useCase;

    public function __construct(CollectionAddUseCase $useCase)
    {
        parent::__construct();

        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('collection:add')
            ->addArgument('movie_ids', InputArgument::IS_ARRAY, 'The movies to add to collection', [])
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLICollectionAddInput($input);
        $cliOutput = new CLICollectionAddOutput($output);

        $this->useCase->process($cliInput, $cliOutput);
    }
}
