<?php
declare(strict_types=1);

namespace App\Command\Search;

use App\Input\CLI\Search\CLISearchInput;
use App\Output\CLI\Search\CLISearchOutput;
use App\View\CLI\SearchViewRenderer;
use N11t\Bundle\MovieBundle\Search\UseCase\SearchUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SearchCommand extends Command
{

    /**
     * @var SearchUseCase
     */
    private $useCase;

    /**
     * @var SearchViewRenderer
     */
    private $viewRenderer;

    public function __construct(
        SearchUseCase $useCase,
        SearchViewRenderer $viewRenderer
    ) {
        parent::__construct();

        $this->useCase = $useCase;
        $this->viewRenderer = $viewRenderer;
    }

    protected function configure()
    {
        $this
            ->setName('search')
            ->setDescription('Search movie online.')
            ->addArgument('query', InputArgument::REQUIRED, 'The title to search for')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $useCaseInput = new CLISearchInput($input);
        $useCaseOutput = new CLISearchOutput();

        $this->useCase->process($useCaseInput, $useCaseOutput);

        $this->viewRenderer->renderList($output, ...$useCaseOutput->results);
    }
}
