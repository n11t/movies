<?php
declare(strict_types=1);

namespace App\Command\Movie;

use App\Output\CLI\Movie\CLIMovieImportOutput;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\DuplicateMovieException;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieImportException;
use App\Input\CLI\Movie\CLIMovieImportInput;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieImportUseCase;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MovieImportCommand extends Command
{

    /**
     * @var MovieImportUseCase
     */
    private $createMovieUseCase;

    public function __construct(MovieImportUseCase $createMovieUseCase)
    {
        parent::__construct();

        $this->createMovieUseCase = $createMovieUseCase;
    }

    protected function configure()
    {
        $this
            ->setName('movie:import')
            ->setDescription('Import movie by Imdb id')
            ->addArgument('imdb_id', InputArgument::REQUIRED, 'The imdb movie id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIMovieImportInput($input);
        $cliOutput = new CLIMovieImportOutput();

        try {
            $this->createMovieUseCase->process($cliInput, $cliOutput);

            if ($cliOutput->view instanceof MovieView) {
                $year = 'N/A';
                $title = $cliOutput->view->getTitle();

                $releaseDate = $cliOutput->view->getReleaseDate();
                if ($releaseDate instanceof \DateTimeInterface) {
                    $year = $releaseDate->format('Y');
                }
                $message = sprintf('Movie %s (%s) imported.', $title, $year);

                $output->writeln("<info>$message</info>");
            }
        } catch (MovieImportException $e) {
            $this->handleException($e, $output);
        }
    }

    private function handleException(MovieImportException $e, OutputInterface $output): void
    {
        if ($e instanceof MovieNotFoundException) {
            $msg = sprintf('<error>Movie not found for %s</error>', $e->getImdbId());
            $output->writeln($msg);

            return;
        } elseif ($e instanceof DuplicateMovieException) {
            $msg = sprintf('<error>Duplicate movie.</error>');
            $output->writeln($msg);

            return;
        }

        $msg = sprintf('<error>%s</error>', $e->getMessage());
        $output->writeln($msg);
    }
}
