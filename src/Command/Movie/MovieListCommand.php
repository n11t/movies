<?php
declare(strict_types=1);

namespace App\Command\Movie;

use App\Input\CLI\Movie\CLIMovieListInput;
use App\Output\CLI\Movie\CLIMovieListOutput;
use App\View\CLI\MovieViewRenderer;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieListUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MovieListCommand extends Command
{

    /**
     * @var MovieListUseCase
     */
    private $movieListUseCase;

    /**
     * @var MovieViewRenderer
     */
    private $movieViewRenderer;

    public function __construct(
        MovieListUseCase $movieListUseCase,
        MovieViewRenderer $movieViewRenderer
    ) {
        parent::__construct();

        $this->movieListUseCase = $movieListUseCase;
        $this->movieViewRenderer = $movieViewRenderer;
    }

    protected function configure()
    {
        $this
            ->setName('movie:list')
            ->setDescription('List all imported movies')
            ->addArgument('query', InputArgument::OPTIONAL, 'The title to search for', null)
            ->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'The limit', 10)
            ->addOption('page', 'p', InputOption::VALUE_REQUIRED, 'The page', 1)
            ->addOption('sort', 's', InputOption::VALUE_REQUIRED, 'Order by property', 'id')
            ->addOption('order', 'o', InputOption::VALUE_REQUIRED, 'Sort direction', 'ASC')
            ->addOption('actor', 'a', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'The actors to search for', null)
            ->addOption('genre', 'g', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'The genre to search for', null)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIMovieListInput($input);
        $cliOutput = new CLIMovieListOutput();

        $this->movieListUseCase->process($cliInput, $cliOutput);

        $this->movieViewRenderer->renderList($output, ...$cliOutput->movies);
    }
}
