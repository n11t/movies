<?php
declare(strict_types=1);

namespace App\Command\Movie;

use App\View\MovieViewTrait;
use App\Output\CLI\Movie\CLIMovieDeleteOutput;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\InCollectionException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\InWatchlistException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieDeleteException;
use App\Input\CLI\Movie\CLIMovieDeleteInput;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieDeleteUseCase;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MovieDeleteCommand extends Command
{
    use MovieViewTrait;

    /**
     * @var MovieDeleteUseCase
     */
    private $deleteMovieUseCase;

    public function __construct(MovieDeleteUseCase $deleteMovieUseCase)
    {
        parent::__construct();

        $this->deleteMovieUseCase = $deleteMovieUseCase;
    }

    protected function configure()
    {
        $this
            ->setName('movie:delete')
            ->setDescription('Delete movie')
            ->addArgument('id', InputArgument::REQUIRED, 'The movie id to delete')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIMovieDeleteInput($input);
        $cliOutput = new CLIMovieDeleteOutput();

        try {
            $this->deleteMovieUseCase->process($cliInput, $cliOutput);

            if ($cliOutput->movie instanceof MovieView) {
                $message = sprintf('Movie %s deleted.', $this->formatMovieName($cliOutput->movie));

                $output->writeln("<info>$message</info>");
            }
        } catch (MovieDeleteException $exception) {
            $this->handleException($exception, $output);
        }
    }

    private function handleException(MovieDeleteException $e, OutputInterface $output): void
    {
        if ($e instanceof MovieNotFoundException) {
            $msg = sprintf('<error>Movie not found for id %d</error>', $e->getId());
            $output->writeln($msg);
        } elseif ($e instanceof InCollectionException) {
            $msg = sprintf('<error>Movie %s still in collection</error>', $this->formatMovieName($e->getView()));

            $output->writeln($msg);
        } elseif ($e instanceof InWatchlistException) {
            $msg = sprintf('<error>Movie %s still on watchlist</error>', $this->formatMovieName($e->getView()));

            $output->writeln($msg);
        } else {
            $msg = sprintf('<error>%s</error>', $e->getMessage());
            $output->writeln($msg);
        }
    }
}
