<?php
declare(strict_types=1);

namespace App\Command\Movie;

use App\Input\CLI\Movie\CLIMovieReimportInput;
use App\Output\CLI\Movie\CLIMovieReimportOutput;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieReimportUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MovieReimportCommand extends Command
{

    /**
     * @var MovieReimportUseCase
     */
    private $useCase;

    public function __construct(MovieReimportUseCase $useCase)
    {
        parent::__construct();

        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('movie:reimport')
            ->setDescription('Reimport all movies or specified by id.')
            ->addArgument('ids', InputArgument::IS_ARRAY, 'The ids to reimport', [])
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Reimport all movies at once')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIMovieReimportInput($input);
        $cliOuput = new CLIMovieReimportOutput($output);

        $this->useCase->process($cliInput, $cliOuput);
    }
}
