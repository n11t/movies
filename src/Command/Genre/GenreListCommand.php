<?php
declare(strict_types=1);

namespace App\Command\Genre;

use N11t\Bundle\MovieBundle\Genre\UseCase\GenreListUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenreListCommand extends Command
{

    /**
     * @var GenreListUseCase
     */
    private $useCase;

    public function __construct(GenreListUseCase $useCase)
    {
        parent::__construct();
        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('genre:list')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CLIGenreListInput($input);
        $cliOuput = new CLIGenreListOutput();
    }
}
