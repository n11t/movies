<?php
declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Input\Frontend\Collection\FrontendCollectionListInput;
use App\Output\Frontend\Collection\FrontendCollectionListOutput;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionListUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CollectionController extends AbstractController
{

    public function listAction(Request $request, CollectionListUseCase $useCase): Response
    {
        $input = new FrontendCollectionListInput($request);
        $output = new FrontendCollectionListOutput($input);

        $useCase->process($input, $output);

        return $this->render('frontend/collection/list.html.twig', [
            'collection' => $output->movies,
        ]);
    }
}
