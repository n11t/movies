<?php
declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Input\Frontend\Watchlist\FrontendWatchlistListInput;
use App\Output\Frontend\Watchlist\FrontendWatchlistListOutput;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistListUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WatchlistController extends AbstractController
{

    public function listAction(Request $request, WatchlistListUseCase $useCase): Response
    {
        $input = new FrontendWatchlistListInput($request);
        $output = new FrontendWatchlistListOutput($input);

        $useCase->process($input, $output);

        return $this->render('frontend/watchlist/list.html.twig', [
            'entries' => $output->entries,
        ]);
    }
}
