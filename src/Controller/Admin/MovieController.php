<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Filter\Movie\ActorFilterFactory;
use App\Filter\Movie\GenreFilterFactory;
use App\Input\Admin\Collection\AdminCollectionAddInput;
use App\Input\Admin\Movie\AdminMovieDeleteInput;
use App\Input\Admin\Movie\AdminMovieImportInput;
use App\Input\Admin\Movie\AdminMovieListInput;
use App\Input\Admin\Watchlist\AdminWatchlistAddInput;
use App\Output\Admin\Collection\AdminCollectionAddOutput;
use App\Output\Admin\Watchlist\AdminWatchlistAddOutput;
use App\View\MovieViewTrait;
use App\Output\Admin\Movie\AdminMovieDeleteOutput;
use App\Output\Admin\Movie\AdminMovieImportOutput;
use App\Output\Admin\Movie\AdminMovieListOutput;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionAddUseCase;
use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Entity\Movie;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\DuplicateMovieException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\InCollectionException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\InWatchlistException;
use N11t\Bundle\MovieBundle\Exception\MovieDelete\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieDeleteException;
use N11t\Bundle\MovieBundle\Exception\MovieImportException;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieDeleteUseCase;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieImportUseCase;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieListUseCase;
use N11t\Bundle\MovieBundle\Movie\View\MovieViewFactory;
use N11t\Bundle\MovieBundle\Repository\ActorRepository;
use N11t\Bundle\MovieBundle\Repository\GenreRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistAddUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class  MovieController extends AbstractController
{
    use MovieViewTrait;

    public function listAction(
        Request $request,
        MovieListUseCase $movieListUseCase,
        GenreFilterFactory $genreFilterFactory,
        ActorFilterFactory $actorFilterFactory
    ): Response {
        $input = new AdminMovieListInput($request);
        $output = new AdminMovieListOutput($input);

        $movieListUseCase->process($input, $output);

        return $this->render('admin/movie/list.html.twig', [
            'movies' => $output->movies,
            'filter' => [
                'query' => $input->getQuery(),
                'genres' => $genreFilterFactory->build(...$input->getGenres()),
                'actors' => $actorFilterFactory->build(...$input->getActors())
            ],
            'pagination' => [
                'page' => $output->getPage(),
                'maxPage' => $output->getMaxPage(),
                'limit' => $output->getLimit(),
            ]
        ]);
    }

    public function watchlistAddAction(
        Request $request,
        WatchlistAddUseCase $useCase,
        TranslatorInterface $translator
    ): Response {
        $input = new AdminWatchlistAddInput($request);
        $output = new AdminWatchlistAddOutput();

        $useCase->process($input, $output);

        foreach ($output->getFlash() as $level => $messages) {
            foreach ($messages as $message) {
                $this->addFlash($level, $translator->trans($message['id'], $message['params'] ?? []));
            }
        }

        return $this->redirectToRoute('admin_movie_list');
    }

    /**
     * @param Request $request
     * @param MovieImportUseCase $movieImportUseCase
     * @param TranslatorInterface $translator
     * @return Response
     * @throws MovieImportException
     */
    public function importAction(
        Request $request,
        MovieImportUseCase $movieImportUseCase,
        TranslatorInterface $translator
    ): Response {
        $input = new AdminMovieImportInput($request);
        $output = new AdminMovieImportOutput();

        $movieImportUseCase->process($input, $output);

        foreach ($output->getFlash() as $level => $messages) {
            foreach ($messages as $message) {
                $this->addFlash($level, $translator->trans($message['id'], $message['params'] ?? []));
            }
        }

        return $this->redirectToRoute('admin_movie_list');
    }

    public function detailAction(
        Request $request,
        MovieRepository $movieRepository,
        MovieViewFactory $movieViewFactory
    ): Response {
        $movie = $movieRepository->find((int)$request->get('id'));

        if (!$movie instanceof Movie) {
            return $this->redirectToRoute('admin_movie_list');
        }

        $view = $movieViewFactory->build($movie);

        return $this->render('admin/movie/detail.html.twig', [
            'movie' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @param MovieDeleteUseCase $useCase
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function deleteAction(
        Request $request,
        MovieDeleteUseCase $useCase,
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $input = new AdminMovieDeleteInput($request);
        $output = new AdminMovieDeleteOutput($urlGenerator);

        try {
            $useCase->process($input, $output);
        } catch (MovieDeleteException $exception) {
            if ($exception instanceof MovieNotFoundException) {
                $this->addFlash('danger', $translator->trans('admin.movie.delete.movieNotFound', [
                    '%id%' => $exception->getId(),
                ]));
            } elseif ($exception instanceof InCollectionException) {
                $this->addFlash('warning', $translator->trans('admin.movie.delete.movieInCollection', [
                    '%movie%' => $this->formatMovieName($exception->getView()),
                ]));
            } elseif ($exception instanceof InWatchlistException) {
                $this->addFlash('warning', $translator->trans('admin.movie.delete.movieInWatchlist', [
                    '%movie%' => $this->formatMovieName($exception->getView()),
                ]));
            } else {
                $this->addFlash('danger', $exception->getMessage());
            }
        }

        foreach ($output->getFlash() as $level => $messages) {
            foreach ($messages as $message) {
                $this->addFlash($level, $translator->trans($message['id'], $message['params'] ?? []));
            }
        }

        return $this->redirectToRoute('admin_movie_list');
    }

    public function collectionAddAction(
        Request $request,
        CollectionAddUseCase $useCase,
        TranslatorInterface $translator
    ): Response {
        $input = new AdminCollectionAddInput($request);
        $output = new AdminCollectionAddOutput();

        $useCase->process($input, $output);

        foreach ($output->moviesAdded as $view) {
            $message = $translator->trans('admin.collection.add.movieAdded', [
                '%movie%' => $this->formatMovieName($view),
            ]);
            $this->addFlash('success', $message);
        }
        foreach ($output->duplicateMovies as $view) {
            $message = $translator->trans('admin.collection.add.movieDuplicate', [
                '%movie%' => $this->formatMovieName($view),
            ]);
            $this->addFlash('warning', $message);
        }
        foreach ($output->notFoundIds as $id) {
            $message = $translator->trans('admin.collection.add.movieNotFound', [
                '%id%' => $id,
            ]);
            $this->addFlash('error', $message);
        }

        return $this->redirectToRoute('admin_movie_list');
    }
}
