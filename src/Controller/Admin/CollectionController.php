<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Filter\Movie\ActorFilterFactory;
use App\Filter\Movie\GenreFilterFactory;
use App\Input\Admin\Collection\AdminCollectionListInput;
use App\Input\Admin\Collection\AdminCollectionRemoveInput;
use App\Input\Admin\Collection\AdminCollectionAddInput;
use App\Output\Admin\Collection\AdminCollectionListOutput;
use App\Output\Admin\Collection\AdminCollectionRemoveOutput;
use App\Output\Admin\Collection\AdminCollectionAddOutput;
use App\View\MovieViewTrait;
use App\Output\Frontend\Collection\FrontendCollectionListOutput;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionAddUseCase;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionListUseCase;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionRemoveUseCase;
use N11t\Bundle\MovieBundle\Collection\View\CollectionEntryView;
use N11t\Bundle\MovieBundle\Entity\Actor;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Genre\UseCase\GenreListUseCase;
use N11t\Bundle\MovieBundle\Repository\ActorRepository;
use N11t\Bundle\MovieBundle\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CollectionController extends AbstractController
{
    use MovieViewTrait;

    public function listAction(
        Request $request,
        CollectionListUseCase $collectionListUseCase,
        GenreFilterFactory $genreFilterFactory,
        ActorFilterFactory $actorFilterFactory
    ): Response {
        $collectionListInput = new AdminCollectionListInput($request);
        $collectionListOutput = new AdminCollectionListOutput($collectionListInput);

        $collectionListUseCase->process($collectionListInput, $collectionListOutput);

        return $this->render('admin/collection/list.html.twig', [
            'collection' => $collectionListOutput->movies,
            'filter' => [
                'query' => $collectionListInput->getQuery(),
                'genres' => $genreFilterFactory->build(...$collectionListInput->getGenres()),
                'actors' => $actorFilterFactory->build(...$collectionListInput->getActors()),
            ],
            'pagination' => [
                'page' => $collectionListOutput->getPage(),
                'maxPage' => $collectionListOutput->getMaxPage(),
                'limit' => $collectionListOutput->getLimit(),
            ]
        ]);
    }

    public function removeAction(
        Request $request,
        CollectionRemoveUseCase $useCase,
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $input = new AdminCollectionRemoveInput($request);
        $output = new AdminCollectionRemoveOutput();

        $useCase->process($input, $output);

        foreach ($output->removedMovies as $view) {
            $message = $translator->trans('admin.collection.remove.movieRemoved', [
                '%movie%' => $this->formatMovieName($view),
                '%link%' => $urlGenerator->generate('admin_collection_add', [
                    'id' => $view->getId(),
                ]),
            ]);
            $this->addFlash('success', $message);
        }
        foreach ($output->notFoundIds as $id) {
            $message = $translator->trans('admin.collection.remove.movieNotFound', [
                '%id%' => $id,
            ]);
            $this->addFlash('error', $message);
        }

        return $this->redirectToRoute('admin_collection_list');
    }

    public function addAction(
        Request $request,
        CollectionAddUseCase $useCase,
        TranslatorInterface $translator
    ): Response {
        $input = new AdminCollectionAddInput($request);
        $output = new AdminCollectionAddOutput();

        $useCase->process($input, $output);

        foreach ($output->moviesAdded as $view) {
            $message = $translator->trans('admin.collection.add.movieAdded', [
                '%movie%' => $this->formatMovieName($view),
            ]);
            $this->addFlash('success', $message);
        }
        foreach ($output->duplicateMovies as $view) {
            $message = $translator->trans('admin.collection.add.movieDuplicate', [
                '%movie%' => $this->formatMovieName($view),
            ]);
            $this->addFlash('warning', $message);
        }
        foreach ($output->notFoundIds as $id) {
            $message = $translator->trans('admin.collection.add.movieNotFound', [
                '%id%' => $id,
            ]);
            $this->addFlash('error', $message);
        }

        return $this->redirectToRoute('admin_collection_list');
    }
}
