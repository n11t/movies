<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Exception\ImportAndWatchlistAddException;
use App\Facade\ImportAndCollectionAddFacadeInterface;
use App\Facade\ImportAndWatchlistAddFacadeInterface;
use App\Input\Admin\Search\AdminSearchInput;
use App\Output\Admin\Search\AdminSearchOutput;
use App\View\MovieViewTrait;
use N11t\Bundle\MovieBundle\Search\UseCase\SearchUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchController extends AbstractController
{
    use MovieViewTrait;

    public function searchAction(Request $request, SearchUseCase $useCase): Response
    {
        $input = new AdminSearchInput($request);
        $output = new AdminSearchOutput();

        $useCase->process($input, $output);

        return $this->render('admin/search.html.twig', [
            'results' => $output->results,
            'query' => $input->getQuery(),
        ]);
    }

    public function watchlistAddAction(
        Request $request,
        ImportAndWatchlistAddFacadeInterface $facade,
        TranslatorInterface $translator
    ): Response {
        $imdbId = $request->get('imdbId');
        $query = $request->get('query');

        try {
            $view = $facade->importAndWatchlistAdd($imdbId);
            $this->addFlash('success', $translator->trans('admin.search.importAndWatchlistAdd.imported', [
                '%movie%' => $this->formatMovieName($view),
            ]));
        } catch (ImportAndWatchlistAddException $exception) {
            $this->addFlash('danger', $exception->getMessage());
        }

        return $this->redirectToRoute('admin_search', [
            'query' => $query,
        ]);
    }

    public function collectionAddAction(
        Request $request,
        ImportAndCollectionAddFacadeInterface $facade,
        TranslatorInterface $translator
    ): Response {
        $imdbId = $request->get('imdbId');
        $query = $request->get('query');

        $view = $facade->importAndCollectionAdd($imdbId);
        $this->addFlash('success', $translator->trans('admin.search.importAndCollectionAdd.imported', [
            '%movie%' => $this->formatMovieName($view),
        ]));

        return $this->redirectToRoute('admin_search', [
            'query' => $query,
        ]);
    }
}
