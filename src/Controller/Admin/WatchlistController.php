<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Filter\Movie\ActorFilterFactory;
use App\Filter\Movie\GenreFilterFactory;
use App\Input\Admin\Watchlist\AdminWatchlistAddInput;
use App\Input\Admin\Watchlist\AdminWatchlistRemoveInput;
use App\Input\Frontend\Watchlist\FrontendWatchlistListInput;
use App\Output\Admin\Watchlist\AdminWatchlistAddOutput;
use App\Output\Admin\Watchlist\AdminWatchlistRemoveOutput;
use App\Output\Frontend\Watchlist\FrontendWatchlistListOutput;
use App\View\MovieViewTrait;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistAddUseCase;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistListUseCase;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistRemoveUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class WatchlistController extends AbstractController
{
    use MovieViewTrait;

    public function listAction(
        Request $request,
        WatchlistListUseCase $useCase,
        GenreFilterFactory $genreFilterFactory,
        ActorFilterFactory $actorFilterFactory
    ): Response {
        $input = new FrontendWatchlistListInput($request);
        $output = new FrontendWatchlistListOutput($input);

        $useCase->process($input, $output);

        return $this->render('admin/watchlist/list.html.twig', [
            'watchlist' => $output->entries,
            'filter' => [
                'query' => $input->getQuery(),
                'genres' => $genreFilterFactory->build(...$input->getGenres()),
                'actors' => $actorFilterFactory->build(...$input->getActors()),
            ],
            'pagination' => [
                'page' => $output->getPage(),
                'maxPage' => $output->getMaxPage(),
            ]
        ]);
    }

    public function removeAction(
        Request $request,
        WatchlistRemoveUseCase $useCase,
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $input = new AdminWatchlistRemoveInput($request);
        $output = new AdminWatchlistRemoveOutput();

        $useCase->process($input, $output);

        foreach ($output->removedMovies as $view) {
            $message = $translator->trans('admin.watchlist.remove.movieRemoved', [
                '%movie%' => $this->formatMovieName($view),
                '%link%' => $urlGenerator->generate('admin_watchlist_add', [
                    'id' => $view->getId(),
                ]),
            ]);
            $this->addFlash('success', $message);
        }
        foreach ($output->notFoundIds as $id) {
            $message = $translator->trans('admin.watchlist.remove.movieNotFound', [
                '%id%' => $id,
            ]);
            $this->addFlash('error', $message);
        }

        return $this->redirectToRoute('admin_watchlist_list');
    }

    public function addAction(
        Request $request,
        WatchlistAddUseCase $useCase,
        TranslatorInterface $translator
    ): Response {
        $input = new AdminWatchlistAddInput($request);
        $output = new AdminWatchlistAddOutput();

        $useCase->process($input, $output);

        foreach ($output->moviesAdded as $view) {
            $message = $translator->trans('admin.watchlist.add.movieAdded', [
                '%movie%' => $this->formatMovieName($view),
            ]);
            $this->addFlash('success', $message);
        }
        foreach ($output->duplicateMovies as $view) {
            $message = $translator->trans('admin.watchlist.add.movieDuplicate', [
                '%movie%' => $this->formatMovieName($view),
            ]);
            $this->addFlash('warning', $message);
        }
        foreach ($output->notFoundIds as $id) {
            $message = $translator->trans('admin.watchlist.add.movieNotFound', [
                '%id%' => $id,
            ]);
            $this->addFlash('error', $message);
        }

        return $this->redirectToRoute('admin_watchlist_list');
    }
}
