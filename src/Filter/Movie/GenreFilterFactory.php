<?php
declare(strict_types=1);

namespace App\Filter\Movie;

use App\Input\Filter\FilterGenreListInput;
use App\Output\Filter\Genre\FilterGenreListOutput;
use N11t\Bundle\MovieBundle\Entity\Genre;
use N11t\Bundle\MovieBundle\Genre\UseCase\GenreListUseCase;

class GenreFilterFactory
{

    /**
     * @var GenreListUseCase
     */
    private $useCase;

    public function __construct(GenreListUseCase $useCase)
    {
        $this->useCase = $useCase;
    }

    public function build(string ...$genres): array
    {
        $input = new FilterGenreListInput();
        $output = new FilterGenreListOutput();

        $this->useCase->process($input, $output);

        return [
            'all' => $output->genres,
            'selected' => $genres,
        ];
    }
}
