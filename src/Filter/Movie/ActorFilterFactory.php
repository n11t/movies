<?php
declare(strict_types=1);

namespace App\Filter\Movie;

use App\Input\Filter\Actor\FilterActorListInput;
use App\Output\Filter\Actor\FilterActorListOutput;
use N11t\Bundle\MovieBundle\Actor\UseCase\ActorListUseCase;

class ActorFilterFactory
{

    /**
     * @var ActorListUseCase
     */
    private $useCase;

    public function __construct(ActorListUseCase $useCase)
    {
        $this->useCase = $useCase;
    }

    public function build(string ...$actors): array
    {
        $input = new FilterActorListInput();
        $output = new FilterActorListOutput($input);

        $this->useCase->process($input, $output);

        return [
            'all' => $output->actors,
            'selected' => $actors,
        ];
    }
}
