<?php
declare(strict_types=1);

namespace App\Facade;

use App\Exception\ImportAndWatchlistAddException;
use App\Input\Facade\FacadeMovieImportInput;
use App\Input\Facade\FacadeWatchlistAddInput;
use App\Output\Facade\FacadeMovieImportOutput;
use App\Output\Facade\FacadeWatchlistAddOutput;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\DuplicateMovieException;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieImportException;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieImportUseCase;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\UseCase\WatchlistAddUseCase;

class ImportAndWatchlistAddFacade implements ImportAndWatchlistAddFacadeInterface
{

    /**
     * @var MovieImportUseCase
     */
    private $movieImportUseCase;

    /**
     * @var WatchlistAddUseCase
     */
    private $watchlistAddUseCase;

    public function __construct(
        MovieImportUseCase $movieImportUseCase,
        WatchlistAddUseCase $watchlistAddUseCase
    )
    {
        $this->movieImportUseCase = $movieImportUseCase;
        $this->watchlistAddUseCase = $watchlistAddUseCase;
    }


    /**
     * @param string $imdbId
     * @return MovieView
     * @throws ImportAndWatchlistAddException
     */
    public function importAndWatchlistAdd(string $imdbId): MovieView
    {
        $movie = $this->movieImport($imdbId);

        $this->watchlistAdd($movie);

        return $movie;
    }

    private function movieImport(string $imdbId): MovieView
    {
        $input = new FacadeMovieImportInput($imdbId);
        $output = new FacadeMovieImportOutput();

        try {
            $this->movieImportUseCase->process($input, $output);
        } catch (MovieImportException $exception) {
            // TODO IMPROVE ERROR HANDLING
            if ($exception instanceof DuplicateMovieException) {
                throw new \RuntimeException($exception->getMessage());
            } elseif ($exception instanceof MovieNotFoundException) {
                throw new \RuntimeException($exception->getMessage());
            }

            throw new \RuntimeException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }

        return $output->movie;
    }

    private function watchlistAdd(MovieView $movie): void
    {
        $input = new FacadeWatchlistAddInput($movie);
        $output = new FacadeWatchlistAddOutput();

        $this->watchlistAddUseCase->process($input, $output);

        if (count($output->notFound) > 0) {
            // TODO IMPROVE ERROR HANDLING!
            $msg = sprintf('Movies %s not found.', json_encode($output->notFound));
            throw new \RuntimeException($msg, 500);
        } elseif (count($output->duplicateMovies) > 0) {
            $msg = sprintf('Movies %s already in watchlist.', json_encode($output->duplicateMovies));
            throw new \RuntimeException($msg, 500);
        } elseif (count($output->moviesAdded) < 1) {
            $msg = sprintf('Movies %s already in watchlist.', json_encode($output->duplicateMovies));
            throw new \RuntimeException($msg, 500);
        }
    }
}
