<?php
declare(strict_types=1);

namespace App\Facade;

use App\Input\Facade\FacadeMovieImportInput;
use App\Input\Facade\FacadeCollectionAddInput;
use App\Output\Facade\FacadeMovieImportOutput;
use App\Output\Facade\FacadeCollectionAddOutput;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\DuplicateMovieException;
use N11t\Bundle\MovieBundle\Exception\MovieCreate\MovieNotFoundException;
use N11t\Bundle\MovieBundle\Exception\MovieImportException;
use N11t\Bundle\MovieBundle\Movie\UseCase\MovieImportUseCase;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Collection\UseCase\CollectionAddUseCase;

class ImportAndCollectionAddFacade implements ImportAndCollectionAddFacadeInterface
{

    /**
     * @var MovieImportUseCase
     */
    private $movieImportUseCase;

    /**
     * @var CollectionAddUseCase
     */
    private $collectionAddUseCase;

    public function __construct(
        MovieImportUseCase $movieImportUseCase,
        CollectionAddUseCase $collectionAddUseCase
    )
    {
        $this->movieImportUseCase = $movieImportUseCase;
        $this->collectionAddUseCase = $collectionAddUseCase;
    }


    /**
     * @param string $imdbId
     * @return MovieView
     */
    public function importAndCollectionAdd(string $imdbId): MovieView
    {
        $movie = $this->movieImport($imdbId);

        $this->collectionAdd($movie);

        return $movie;
    }

    private function movieImport(string $imdbId): MovieView
    {
        $input = new FacadeMovieImportInput($imdbId);
        $output = new FacadeMovieImportOutput();

        try {
            $this->movieImportUseCase->process($input, $output);
        } catch (MovieImportException $exception) {
            // TODO IMPROVE ERROR HANDLING
            if ($exception instanceof DuplicateMovieException) {
                throw new \RuntimeException($exception->getMessage());
            } elseif ($exception instanceof MovieNotFoundException) {
                throw new \RuntimeException($exception->getMessage());
            }

            throw new \RuntimeException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }

        return $output->movie;
    }

    private function collectionAdd(MovieView $movie): void
    {
        $input = new FacadeCollectionAddInput($movie);
        $output = new FacadeCollectionAddOutput();

        $this->collectionAddUseCase->process($input, $output);

        if (count($output->notFound) > 0) {
            // TODO IMPROVE ERROR HANDLING!
            $msg = sprintf('Movies %s not found.', json_encode($output->notFound));
            throw new \RuntimeException($msg, 500);
        } elseif (count($output->duplicateMovies) > 0) {
            $msg = sprintf('Movies %s already in collection.', json_encode($output->duplicateMovies));
            throw new \RuntimeException($msg, 500);
        } elseif (count($output->moviesAdded) < 1) {
            $msg = sprintf('Movies %s already in collection.', json_encode($output->duplicateMovies));
            throw new \RuntimeException($msg, 500);
        }
    }
}
