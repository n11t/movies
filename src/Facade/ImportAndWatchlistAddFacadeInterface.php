<?php
declare(strict_types=1);

namespace App\Facade;

use App\Exception\ImportAndWatchlistAddException;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface ImportAndWatchlistAddFacadeInterface
{

    /**
     * @param string $imdbId
     * @return MovieView
     * @throws ImportAndWatchlistAddException
     */
    public function importAndWatchlistAdd(string $imdbId): MovieView;
}
