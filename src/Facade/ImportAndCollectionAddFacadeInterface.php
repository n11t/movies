<?php
declare(strict_types=1);

namespace App\Facade;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;

interface ImportAndCollectionAddFacadeInterface
{

    /**
     * @param string $imdbId
     * @return MovieView
     */
    public function importAndCollectionAdd(string $imdbId): MovieView;
}
