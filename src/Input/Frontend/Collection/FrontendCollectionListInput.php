<?php
declare(strict_types=1);

namespace App\Input\Frontend\Collection;

use App\Input\HTTP\Collection\HTTPCollectionListInput;

class FrontendCollectionListInput extends HTTPCollectionListInput
{

    public function getOrderBy(): array
    {
        $order = $this->request->get('order', 'ASC');
        $sort = $this->request->get('sort', 'title');

        return [
            $sort => $order
        ];
    }

    public function getGenres(): ?array
    {
        return $this->request->get('genres', null);
    }

    public function getActors(): ?array
    {
        return $this->request->get('actors', null);
    }
}
