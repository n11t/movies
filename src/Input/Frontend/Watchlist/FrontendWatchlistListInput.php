<?php
declare(strict_types=1);

namespace App\Input\Frontend\Watchlist;

use App\Input\HTTP\Watchlist\HttpWatchlistListInput;

class FrontendWatchlistListInput extends HttpWatchlistListInput
{

    public function getOrderBy(): array
    {
        $order = $this->request->get('order', 'ASC');
        $sort = $this->request->get('sort', 'title');

        return [
            $sort => $order
        ];
    }

    public function getGenres(): ?array
    {
        return $this->request->get('genres', []);
    }

    public function getActors(): ?array
    {
        return $this->request->get('actors', []);
    }
}
