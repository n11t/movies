<?php
declare(strict_types=1);

namespace App\Input\Admin\Search;

use App\Input\HTTP\Search\HttpSearchInput;

class AdminSearchInput extends HttpSearchInput
{
}
