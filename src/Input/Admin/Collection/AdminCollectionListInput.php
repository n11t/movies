<?php
declare(strict_types=1);

namespace App\Input\Admin\Collection;

use App\Input\HTTP\Collection\HTTPCollectionListInput;

class AdminCollectionListInput extends HTTPCollectionListInput
{

    public function getLimit(): ?int
    {
        $limit = parent::getLimit();
        if (!is_int($limit)) {
            return 10;
        }

        return $limit;
    }

    public function getGenres(): ?array
    {
        $genres = $this->request->get('genres', []);
        if (!is_array($genres)) {
            return [
                $genres
            ];
        }

        return $genres;
    }

    public function getActors(): ?array
    {
        $genres = $this->request->get('actors', []);
        if (!is_array($genres)) {
            return [
                $genres
            ];
        }

        return $genres;
    }
}
