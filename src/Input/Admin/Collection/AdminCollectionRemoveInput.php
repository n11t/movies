<?php
declare(strict_types=1);

namespace App\Input\Admin\Collection;

use App\Input\HTTP\Collection\HttpCollectionRemoveInput;

class AdminCollectionRemoveInput extends HttpCollectionRemoveInput
{
}
