<?php
declare(strict_types=1);

namespace App\Input\Admin\Watchlist;

use App\Input\HTTP\Watchlist\HttpWatchlistAddInput;

class AdminWatchlistAddInput extends HttpWatchlistAddInput
{
}
