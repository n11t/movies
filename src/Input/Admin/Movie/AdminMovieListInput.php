<?php
declare(strict_types=1);

namespace App\Input\Admin\Movie;

use App\Input\HTTP\Movie\HttpMovieListInput;

class AdminMovieListInput extends HttpMovieListInput
{

    public function getLimit(): ?int
    {
        $limit = parent::getLimit();
        if (is_null($limit)) {
            return 15;
        }

        return $limit;
    }
}
