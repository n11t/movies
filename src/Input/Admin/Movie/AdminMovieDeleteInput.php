<?php
declare(strict_types=1);

namespace App\Input\Admin\Movie;

use App\Input\HTTP\Movie\HttpMovieDeleteInput;

class AdminMovieDeleteInput extends HttpMovieDeleteInput
{
}
