<?php
declare(strict_types=1);

namespace App\Input\Admin\Movie;

use N11t\Bundle\MovieBundle\Movie\Input\MovieImportInputInterface;
use Symfony\Component\HttpFoundation\Request;

class AdminMovieImportInput implements MovieImportInputInterface
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getImdbId(): string
    {
        return $this->request->get('imdbId', '');
    }
}
