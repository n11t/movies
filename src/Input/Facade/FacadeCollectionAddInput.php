<?php
declare(strict_types=1);

namespace App\Input\Facade;

use N11t\Bundle\MovieBundle\Collection\Input\CollectionAddInputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class FacadeCollectionAddInput implements CollectionAddInputInterface
{

    /**
     * @var MovieView
     */
    private $view;

    public function __construct(MovieView $view)
    {
        $this->view = $view;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return [
            (int)$this->view->getId(),
        ];
    }
}
