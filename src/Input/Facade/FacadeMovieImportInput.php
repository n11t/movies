<?php
declare(strict_types=1);

namespace App\Input\Facade;

use N11t\Bundle\MovieBundle\Movie\Input\MovieImportInputInterface;

class FacadeMovieImportInput implements MovieImportInputInterface
{

    /**
     * @var string
     */
    private $imdbId;

    public function __construct(string $imdbId)
    {
        $this->imdbId = $imdbId;
    }

    public function getImdbId(): string
    {
        return $this->imdbId;
    }
}
