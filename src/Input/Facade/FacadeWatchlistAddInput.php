<?php
declare(strict_types=1);

namespace App\Input\Facade;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistAddInputInterface;

class FacadeWatchlistAddInput implements WatchlistAddInputInterface
{

    /**
     * @var MovieView
     */
    private $movieView;

    public function __construct(MovieView $movieView)
    {
        $this->movieView = $movieView;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return [
            $this->movieView->getId(),
        ];
    }
}
