<?php
declare(strict_types=1);

namespace App\Input\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Input\MovieImportInputInterface;
use Symfony\Component\Console\Input\InputInterface;

class CLIMovieImportInput implements MovieImportInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getImdbId(): string
    {
        return $this->input->getArgument('imdb_id');
    }
}
