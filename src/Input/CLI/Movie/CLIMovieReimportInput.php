<?php
declare(strict_types=1);

namespace App\Input\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Input\MovieReimportInputInterface;
use Symfony\Component\Console\Input\InputInterface;

class CLIMovieReimportInput implements MovieReimportInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getMovieIds(): ?array
    {
        $reimportAll = $this->input->getOption('all');

        if ($reimportAll) {
            return null;
        }

        $ids = [];
        foreach ($this->input->getArgument('ids') as $id) {
            $ids[] = (int)$id;
        }

        return $ids;
    }
}
