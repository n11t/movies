<?php
declare(strict_types=1);

namespace App\Input\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Input\MovieDeleteInputInterface;
use Symfony\Component\Console\Input\InputInterface;

class CLIMovieDeleteInput implements MovieDeleteInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getId(): int
    {
        return (int)$this->input->getArgument('id');
    }
}
