<?php
declare(strict_types=1);

namespace App\Input\CLI\Movie;

use App\Input\CLI\AbstractCLIListInput;
use N11t\Bundle\MovieBundle\Movie\Input\MovieListInputInterface;

class CLIMovieListInput extends AbstractCLIListInput implements MovieListInputInterface
{
    /**
     * @return int[]|null
     */
    public function getGenres(): ?array
    {
        $genres = $this->input->getOption('genre');
        if (!is_array($genres)) {
            return null;
        }

        $ids = [];
        foreach ($genres as $genre) {
            $ids[] = (int)$genre;
        }

        return $ids;
    }

    /**
     * @return int[]|null
     */
    public function getActors(): ?array
    {
        $actors = $this->input->getOption('actor');

        $ids = [];
        foreach ($actors as $actor) {
            $ids[] = (int)$actor;
        }

        return $ids;
    }
}
