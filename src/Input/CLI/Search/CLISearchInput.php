<?php
declare(strict_types=1);

namespace App\Input\CLI\Search;

use N11t\Bundle\MovieBundle\Search\Input\SearchInputInterface;
use Symfony\Component\Console\Input\InputInterface;

class CLISearchInput implements SearchInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getQuery(): string
    {
        return $this->input->getArgument('query');
    }
}
