<?php
declare(strict_types=1);

namespace App\Input\CLI\Watchlist;

use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistAddInputInterface;
use Symfony\Component\Console\Input\InputInterface;

class CLIWatchlistAddInput implements WatchlistAddInputInterface
{
    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return $this->input->getArgument('movie_ids');
    }
}
