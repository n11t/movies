<?php
declare(strict_types=1);

namespace App\Input\CLI\Watchlist;

use App\Input\CLI\AbstractCLIListInput;
use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistListInputInterface;

class CLIWatchlistListInput extends AbstractCLIListInput implements WatchlistListInputInterface
{

    /**
     * @return int[]|null
     */
    public function getGenres(): ?array
    {
        $genres = $this->input->getOption('genre');

        $ids = [];
        foreach ($genres as $genre) {
            $ids[] = (int)$genre;
        }

        return $ids;
    }

    /**
     * @return int[]|null
     */
    public function getActors(): ?array
    {
        $actors = $this->input->getOption('actor');

        $ids = [];
        foreach ($actors as $actor) {
            $ids[] = (int)$actor;
        }

        return $ids;
    }
}
