<?php
declare(strict_types=1);

namespace App\Input\CLI;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;
use Symfony\Component\Console\Input\InputInterface;

abstract class AbstractCLIListInput implements ListInputInterface
{

    /**
     * @var InputInterface
     */
    protected $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getQuery(): ?string
    {
        return $this->input->getArgument('query');
    }

    public function getOrderBy(): array
    {
        $sort = $this->input->getOption('sort');
        $order = $this->input->getOption('order');

        return [
            $sort => $order,
        ];
    }

    public function getLimit(): int
    {
        return (int)$this->input->getOption('limit');
    }

    public function getPage(): int
    {
        return (int)$this->input->getOption('page');
    }
}
