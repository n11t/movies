<?php
declare(strict_types=1);

namespace App\Input\CLI\Collection;

use N11t\Bundle\MovieBundle\Collection\Input\CollectionAddInputInterface;
use Symfony\Component\Console\Input\InputInterface;

class CLICollectionAddInput implements CollectionAddInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        $ids = [];
        foreach ($this->input->getArgument('movie_ids') as $id) {
            $ids[] = (int)$id;
        }

        return $ids;
    }
}
