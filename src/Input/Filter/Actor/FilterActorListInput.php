<?php
declare(strict_types=1);

namespace App\Input\Filter\Actor;

use N11t\Bundle\MovieBundle\Actor\Input\ActorListInputInterface;

class FilterActorListInput implements ActorListInputInterface
{
    public function getQuery(): ?string
    {
        return null;
    }

    public function getOrderBy(): array
    {
        return [
            'name' => 'ASC',
        ];
    }

    public function getLimit(): ?int
    {
        return null;
    }

    public function getPage(): ?int
    {
        return null;
    }
}
