<?php
declare(strict_types=1);

namespace App\Input\Filter;

use N11t\Bundle\MovieBundle\Genre\Input\GenreListInputInterface;

class FilterGenreListInput implements GenreListInputInterface
{

    public function getQuery(): ?string
    {
        return null;
    }

    public function getOrderBy(): array
    {
        return [
            'name' => 'ASC',
        ];
    }

    public function getLimit(): ?int
    {
        return null;
    }

    public function getPage(): ?int
    {
        return null;
    }
}
