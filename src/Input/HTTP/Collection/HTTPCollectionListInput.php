<?php
declare(strict_types=1);

namespace App\Input\HTTP\Collection;

use App\Input\HTTP\AbstractHttpListInput;
use N11t\Bundle\MovieBundle\Collection\Input\CollectionListInputInterface;

abstract class HTTPCollectionListInput extends AbstractHttpListInput implements CollectionListInputInterface
{
}
