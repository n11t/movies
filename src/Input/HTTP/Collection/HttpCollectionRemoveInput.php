<?php
declare(strict_types=1);

namespace App\Input\HTTP\Collection;

use N11t\Bundle\MovieBundle\Collection\Input\CollectionRemoveInputInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class HttpCollectionRemoveInput implements CollectionRemoveInputInterface
{

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return [
            (int)$this->request->get('id'),
        ];
    }
}
