<?php
declare(strict_types=1);

namespace App\Input\HTTP;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractHttpListInput implements ListInputInterface
{

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getQuery(): ?string
    {
        $query = $this->request->get('query', null);
        if (is_null($query) || strlen($query) < 1) {
            return null;
        }

        return $query;
    }

    public function getOrderBy(): array
    {
        $order = $this->request->get('order', 'ASC');
        $sort = $this->request->get('sort', 'id');

        return [
            $sort => $order
        ];
    }

    public function getLimit(): ?int
    {
        $limit = $this->request->get('limit', null);
        if (!is_null($limit)) {
            return (int) $limit;
        }

        return null;
    }

    public function getPage(): int
    {
        return (int)$this->request->get('page', 1);
    }
}
