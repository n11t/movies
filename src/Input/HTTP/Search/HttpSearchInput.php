<?php
declare(strict_types=1);

namespace App\Input\HTTP\Search;

use N11t\Bundle\MovieBundle\Search\Input\SearchInputInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class HttpSearchInput implements SearchInputInterface
{

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getQuery(): string
    {
        return $this->request->get('query', '');
    }
}
