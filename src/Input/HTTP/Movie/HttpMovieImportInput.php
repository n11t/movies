<?php
declare(strict_types=1);

namespace App\Input\HTTP\Movie;

use N11t\Bundle\MovieBundle\Movie\Input\MovieImportInputInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class HttpMovieImportInput implements MovieImportInputInterface
{

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getImdbId(): string
    {
        return $this->request->get('imdbId', '');
    }
}
