<?php
declare(strict_types=1);

namespace App\Input\HTTP\Movie;

use App\Input\HTTP\AbstractHttpListInput;
use N11t\Bundle\MovieBundle\Movie\Input\MovieListInputInterface;

abstract class HttpMovieListInput extends AbstractHttpListInput implements MovieListInputInterface
{

    public function getGenres(): ?array
    {
        return $this->request->get('genres', []);
    }

    public function getActors(): ?array
    {
        return $this->request->get('actors', []);
    }
}
