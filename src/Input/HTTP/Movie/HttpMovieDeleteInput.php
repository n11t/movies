<?php
declare(strict_types=1);

namespace App\Input\HTTP\Movie;

use N11t\Bundle\MovieBundle\Movie\Input\MovieDeleteInputInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class HttpMovieDeleteInput implements MovieDeleteInputInterface
{

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getId(): int
    {
        return (int)$this->request->get('id', -1);
    }
}

