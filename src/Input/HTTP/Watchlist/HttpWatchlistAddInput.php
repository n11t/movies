<?php
declare(strict_types=1);

namespace App\Input\HTTP\Watchlist;

use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistAddInputInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class HttpWatchlistAddInput implements WatchlistAddInputInterface
{

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return [
            (int)$this->request->get('id'),
        ];
    }
}

