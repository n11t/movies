<?php
declare(strict_types=1);

namespace App\Input\HTTP\Watchlist;

use App\Input\HTTP\AbstractHttpListInput;
use N11t\Bundle\MovieBundle\Watchlist\Input\WatchlistListInputInterface;

abstract class HttpWatchlistListInput extends AbstractHttpListInput implements WatchlistListInputInterface
{
}
