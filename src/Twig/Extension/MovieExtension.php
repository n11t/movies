<?php
declare(strict_types=1);

namespace App\Twig\Extension;

use App\Formatter\Movie\MovieFormatter;
use App\UrlGenerator\ImdbUrlGenerator;
use App\UrlGenerator\TrailerUrlGenerator;
use N11t\Bundle\MovieBundle\Repository\CollectionRepository;
use N11t\Bundle\MovieBundle\Repository\MovieRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MovieExtension extends AbstractExtension
{

    /**
     * @var MovieFormatter
     */
    private $movieFormatter;

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;
    /**
     * @var TrailerUrlGenerator
     */
    private $trailerUrlGenerator;
    /**
     * @var ImdbUrlGenerator
     */
    private $imdbUrlGenerator;

    public function __construct(
        MovieFormatter $movieFormatter,
        CollectionRepository $collectionRepository,
        TrailerUrlGenerator $trailerUrlGenerator,
        ImdbUrlGenerator $imdbUrlGenerator
    ) {
        $this->movieFormatter = $movieFormatter;
        $this->collectionRepository = $collectionRepository;
        $this->trailerUrlGenerator = $trailerUrlGenerator;
        $this->imdbUrlGenerator = $imdbUrlGenerator;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('format_movie_length', [$this->movieFormatter, 'formatMovieLength']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('movie_count', [$this->collectionRepository, 'countAll']),
            new TwigFunction('movie_trailer_url', [$this->trailerUrlGenerator, 'generate']),
            new TwigFunction('movie_imdb_url', [$this->imdbUrlGenerator, 'generate']),
        ];
    }
}
