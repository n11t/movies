<?php
declare(strict_types=1);

namespace App\Output\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Output\MovieReimportOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Output\OutputInterface;

class CLIMovieReimportOutput implements MovieReimportOutputInterface
{

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function movieNotFound(string $imdbId): void
    {
        $message = sprintf('No movie found for imdb id %s', $imdbId);
        $this->output->writeln("<error>$message</error>");
    }

    public function reimported(MovieView $view): void
    {
        $year = 'N/A';
        if ($view->getReleaseDate() instanceof \DateTimeInterface) {
            $year = $view->getReleaseDate()->format('Y');
        }

        $message = sprintf('%s (%s) reimported', $view->getTitle(), $year);
        $this->output->writeln("<info>$message</info>");
    }

    public function movieForIdNotFound(int $id): void
    {
        $message = sprintf('No movie found for id %s', $id);
        $this->output->writeln("<error>$message</error>");
    }
}
