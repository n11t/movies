<?php
declare(strict_types=1);

namespace App\Output\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Output\MovieImportOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class CLIMovieImportOutput implements MovieImportOutputInterface
{

    /**
     * @var MovieView|null
     */
    public $view = null;

    public function setMovie(MovieView $movie): void
    {
        $this->view = $movie;
    }
}
