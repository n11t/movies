<?php
declare(strict_types=1);

namespace App\Output\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Output\MovieListOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class CLIMovieListOutput implements MovieListOutputInterface
{

    /**
     * @var MovieView[]
     */
    public $movies = [];

    /**
     * @var int|null
     */
    public $count = null;

    public function addMovie(MovieView $view): void
    {
        $this->movies[] = $view;
    }

    public function setCount(int $count): void
    {
        $this->count = $count;
    }
}
