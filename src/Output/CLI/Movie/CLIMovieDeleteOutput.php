<?php
declare(strict_types=1);

namespace App\Output\CLI\Movie;

use N11t\Bundle\MovieBundle\Movie\Output\MovieDeleteOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class CLIMovieDeleteOutput implements MovieDeleteOutputInterface
{

    /**
     * @var MovieView|null
     */
    public $movie = null;

    public function setMovie(MovieView $view): void
    {
        $this->movie = $view;
    }
}
