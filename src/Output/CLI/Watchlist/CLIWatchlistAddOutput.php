<?php
declare(strict_types=1);

namespace App\Output\CLI\Watchlist;

use App\View\MovieViewTrait;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistAddOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CLIWatchlistAddOutput implements WatchlistAddOutputInterface
{
    use MovieViewTrait;

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function movieNotFound(int $id): void
    {
        $message = sprintf('No movie found for id %d', $id);

        $this->output->writeln("<error>$message</error>");
    }

    public function movieAdded(MovieView $view): void
    {
        $message = sprintf('%s added.', $this->formatMovieName($view));

        $this->output->writeln("<info>$message</info>");
    }

    public function movieIsInWatchlist(MovieView $view): void
    {
        $message = sprintf('Movie %s is already on watchlist', $this->formatMovieName($view));

        $this->output->writeln("<error>$message</error>");
    }
}
