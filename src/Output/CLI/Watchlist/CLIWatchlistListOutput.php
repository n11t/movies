<?php
declare(strict_types=1);

namespace App\Output\CLI\Watchlist;

use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistListOutputInterface;
use N11t\Bundle\MovieBundle\Watchlist\View\WatchlistEntryView;

class CLIWatchlistListOutput implements WatchlistListOutputInterface
{

    /**
     * @var int|null
     */
    public $count = null;

    /**
     * @var WatchlistEntryView[]
     */
    public $entries = [];

    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    public function addEntry(WatchlistEntryView $view): void
    {
        $this->entries[] = $view;
    }
}
