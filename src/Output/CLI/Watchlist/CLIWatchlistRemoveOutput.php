<?php
declare(strict_types=1);

namespace App\Output\CLI\Watchlist;

use App\View\MovieViewTrait;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistRemoveOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CLIWatchlistRemoveOutput implements WatchlistRemoveOutputInterface
{
    use MovieViewTrait;

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function entryNotFoundForId(int $id): void
    {
        $message = sprintf('No entry found for id %d', $id);

        $this->output->writeln("<error>$message</error>");
    }

    public function movieRemoved(MovieView $view): void
    {
        $message = sprintf('%s removed from watchlist', $this->formatMovieName($view));

        $this->output->writeln("<info>$message</info>");
    }
}
