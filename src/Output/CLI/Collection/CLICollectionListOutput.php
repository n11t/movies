<?php
declare(strict_types=1);

namespace App\Output\CLI\Collection;

use N11t\Bundle\MovieBundle\Collection\View\CollectionEntryView;
use N11t\Bundle\MovieBundle\Collection\Output\CollectionListOutputInterface;

class CLICollectionListOutput implements CollectionListOutputInterface
{

    /**
     * @var int|null
     */
    public $count = null;

    /**
     * @var CollectionEntryView[]
     */
    public $movies = [];

    public function addEntry(CollectionEntryView $view): void
    {
        $this->movies[] = $view;
    }

    public function setCount(int $count): void
    {
        $this->count = $count;
    }
}
