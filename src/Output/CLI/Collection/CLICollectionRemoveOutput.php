<?php
declare(strict_types=1);

namespace App\Output\CLI\Collection;

use App\View\MovieViewTrait;
use N11t\Bundle\MovieBundle\Collection\Output\CollectionRemoveOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Output\OutputInterface;

class CLICollectionRemoveOutput implements CollectionRemoveOutputInterface
{
    use MovieViewTrait;

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function entryNotFoundForId(int $id): void
    {
        $message = sprintf('No entry found for id %d', $id);

        $this->output->writeln("<error>$message</error>");
    }

    public function movieRemoved(MovieView $view): void
    {
        $message = sprintf('%s removed from collection', $this->formatMovieName($view));

        $this->output->writeln("<info>$message</info>");
    }
}
