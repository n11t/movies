<?php
declare(strict_types=1);

namespace App\Output\CLI\Collection;

use N11t\Bundle\MovieBundle\Collection\Output\CollectionAddOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use Symfony\Component\Console\Output\OutputInterface;

class CLICollectionAddOutput implements CollectionAddOutputInterface
{

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function movieNotFound(int $id): void
    {
        $message = sprintf('No movie found for id %d', $id);

        $this->output->writeln("<error>$message</error>");
    }

    public function movieAdded(MovieView $view): void
    {
        $message = sprintf('%s added.', $this->formatView($view));

        $this->output->writeln("<info>$message</info>");
    }

    public function movieIsInCollection(MovieView $view): void
    {
        $message = sprintf('Movie %s is already in collection', $this->formatView($view));

        $this->output->writeln("<error>$message</error>");
    }

    private function formatView(MovieView $view): string
    {
        $year = 'N/A';
        if ($view->getReleaseDate() instanceof \DateTimeInterface) {
            $year = $view->getReleaseDate()->format('Y');
        }

        return sprintf('%s (%s)', $view->getTitle(), $year);
    }
}
