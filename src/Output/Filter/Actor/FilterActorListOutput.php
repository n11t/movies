<?php
declare(strict_types=1);

namespace App\Output\Filter\Actor;

use App\Output\HTTP\AbstractHttpListOutput;
use N11t\Bundle\MovieBundle\Actor\Output\ActorListOutputInterface;
use N11t\Bundle\MovieBundle\Actor\View\ActorView;

class FilterActorListOutput extends AbstractHttpListOutput implements ActorListOutputInterface
{

    public $actors = [];

    public function addActor(ActorView $view): void
    {
        $this->actors[] = $view;
    }
}
