<?php
declare(strict_types=1);

namespace App\Output\Filter\Genre;

use N11t\Bundle\MovieBundle\Genre\Output\GenreListOutputInterface;
use N11t\Bundle\MovieBundle\Genre\View\GenreView;

class FilterGenreListOutput implements GenreListOutputInterface
{

    /**
     * @var int|null
     */
    public $count = null;

    /**
     * @var GenreView[]
     */
    public $genres = [];

    public function addGenre(GenreView $view): void
    {
        $this->genres[] = $view;
    }

    public function setCount(int $count): void
    {
        $this->count = $count;
    }
}
