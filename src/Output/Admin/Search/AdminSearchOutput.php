<?php
declare(strict_types=1);

namespace App\Output\Admin\Search;

use App\Output\HTTP\Search\HttpSearchOutput;

class AdminSearchOutput extends HttpSearchOutput
{
}
