<?php
declare(strict_types=1);

namespace App\Output\Admin\Movie;

use App\Output\HTTP\Movie\HttpMovieDeleteOutput;
use App\View\MovieViewTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdminMovieDeleteOutput extends HttpMovieDeleteOutput
{
    use MovieViewTrait;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function getFlash()
    {
        return [
            'success' => [
                [
                    'id' => 'admin.movie.delete.movieDeleted',
                    'params' => [
                        '%movie%' => $this->formatMovieName($this->movie),
                        '%link%' => $this->urlGenerator->generate('admin_movie_import', [
                            'imdbId' => $this->movie->getImdbId(),
                        ]),
                    ]
                ]
            ]
        ];
    }
}
