<?php
declare(strict_types=1);

namespace App\Output\Admin\Movie;

use App\Output\HTTP\Movie\HttpMovieListOutput;

class AdminMovieListOutput extends HttpMovieListOutput
{
}
