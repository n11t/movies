<?php
declare(strict_types=1);

namespace App\Output\Admin\Movie;

use App\Output\HTTP\Movie\HttpMovieImportOutput;
use App\View\MovieViewTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminMovieImportOutput extends HttpMovieImportOutput
{
    use MovieViewTrait;

    public function getFlash(): array
    {
        return [
            'success' =>  [
                'admin.movie.import.movieImported' => [
            '       %movie%' => $this->formatMovieName($this->movie),
                ],
            ],
        ];
    }
}
