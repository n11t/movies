<?php
declare(strict_types=1);

namespace App\Output\Admin\Collection;

use App\Output\HTTP\Collection\HttpCollectionListOutput;

class AdminCollectionListOutput extends HttpCollectionListOutput
{
}
