<?php
declare(strict_types=1);

namespace App\Output\Admin\Watchlist;

use App\Output\HTTP\Watchlist\HttpWatchlistAddOutput;
use App\View\MovieViewTrait;

class AdminWatchlistAddOutput extends HttpWatchlistAddOutput
{
    use MovieViewTrait;

    public function getFlash(): array
    {
        $success = [];
        $warning = [];
        $error = [];
        foreach ($this->moviesAdded as $view) {
            $success[] = [
                'id' => 'admin.watchlist.add.movieAdded',
                'params' => [
                    '%movie%' => $this->formatMovieName($view),
                ]
            ];
        }
        foreach ($this->duplicateMovies as $view) {
            $warning[] = [
                'id' => 'admin.watchlist.add.movieDuplicate',
                'params' => [
                    '%movie%' => $this->formatMovieName($view),
                ]
            ];
        }
        foreach ($this->notFoundIds as $id) {
            $error[] = [
                'id' => 'admin.watchlist.add.movieNotFound',
                'params' => [
                    '%id%' => $id,
                ]
            ];
        }

        return [
            'success' => $success,
            'warning' => $warning,
            'error' => $error,
        ];
    }
}
