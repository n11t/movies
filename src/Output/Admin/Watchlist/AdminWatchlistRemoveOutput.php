<?php
declare(strict_types=1);

namespace App\Output\Admin\Watchlist;

use App\Output\HTTP\Watchlist\HttpWatchlistRemoveOutput;

class AdminWatchlistRemoveOutput extends HttpWatchlistRemoveOutput
{
}
