<?php
declare(strict_types=1);

namespace App\Output\Facade;

use N11t\Bundle\MovieBundle\Collection\Output\CollectionAddOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

class FacadeCollectionAddOutput implements CollectionAddOutputInterface
{
    /**
     * @var int[]
     */
    public $notFound = [];

    /**
     * @var MovieView[]
     */
    public $moviesAdded = [];

    /**
     * @var MovieView[]
     */
    public $duplicateMovies = [];

    public function movieNotFound(int $id): void
    {
        $this->notFound[] = $id;
    }

    public function movieAdded(MovieView $view): void
    {
        $this->moviesAdded[] = $view;
    }

    public function movieIsInCollection(MovieView $view): void
    {
        $this->duplicateMovies[] = $view;
    }
}
