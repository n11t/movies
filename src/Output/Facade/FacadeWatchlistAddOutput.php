<?php
declare(strict_types=1);

namespace App\Output\Facade;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistAddOutputInterface;

class FacadeWatchlistAddOutput implements WatchlistAddOutputInterface
{

    /**
     * @var MovieView[]
     */
    public $moviesAdded = [];

    /**
     * @var MovieView[]
     */
    public $duplicateMovies = [];

    /**
     * @var int[]
     */
    public $notFound = [];

    public function movieNotFound(int $id): void
    {
        $this->notFound[] = $id;
    }

    public function movieIsInWatchlist(MovieView $view): void
    {
        $this->duplicateMovies[] = $view;
    }

    public function movieAdded(MovieView $view): void
    {
        $this->moviesAdded[] = $view;
    }
}
