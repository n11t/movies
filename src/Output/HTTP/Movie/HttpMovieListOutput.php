<?php
declare(strict_types=1);

namespace App\Output\HTTP\Movie;

use App\Output\HTTP\AbstractHttpListOutput;
use N11t\Bundle\MovieBundle\Movie\Output\MovieListOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

abstract class HttpMovieListOutput extends AbstractHttpListOutput implements MovieListOutputInterface
{

    /**
     * @var MovieView[]
     */
    public $movies = [];

    public function addMovie(MovieView $view): void
    {
        $this->movies[] = $view;
    }
}
