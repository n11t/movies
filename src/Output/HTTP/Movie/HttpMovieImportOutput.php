<?php
declare(strict_types=1);

namespace App\Output\HTTP\Movie;

use N11t\Bundle\MovieBundle\Movie\Output\MovieImportOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

abstract class HttpMovieImportOutput implements MovieImportOutputInterface
{

    /**
     * @var MovieView|null
     */
    public $movie = null;

    public function setMovie(MovieView $movie): void
    {
        $this->movie = $movie;
    }
}
