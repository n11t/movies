<?php
declare(strict_types=1);

namespace App\Output\HTTP\Watchlist;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistAddOutputInterface;

abstract class HttpWatchlistAddOutput implements WatchlistAddOutputInterface
{

    /**
     * @var int[]
     */
    public $notFoundIds = [];

    /**
     * @var MovieView[]
     */
    public $moviesAdded = [];

    /**
     * @var MovieView[]
     */
    public $duplicateMovies = [];

    public function movieNotFound(int $id): void
    {
        $this->notFoundIds[] = $id;
    }

    public function movieAdded(MovieView $view): void
    {
        $this->moviesAdded[] = $view;
    }

    public function movieIsInWatchlist(MovieView $view): void
    {
        $this->duplicateMovies[] = $view;
    }
}
