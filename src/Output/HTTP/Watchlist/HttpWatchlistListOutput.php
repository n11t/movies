<?php
declare(strict_types=1);

namespace App\Output\HTTP\Watchlist;

use App\Output\HTTP\AbstractHttpListOutput;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistListOutputInterface;
use N11t\Bundle\MovieBundle\Watchlist\View\WatchlistEntryView;

abstract class HttpWatchlistListOutput extends AbstractHttpListOutput implements WatchlistListOutputInterface
{

    /**
     * @var WatchlistEntryView[]
     */
    public $entries = [];

    public function addEntry(WatchlistEntryView $view): void
    {
        $this->entries[] = $view;
    }
}
