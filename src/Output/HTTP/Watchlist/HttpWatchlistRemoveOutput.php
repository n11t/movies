<?php
declare(strict_types=1);

namespace App\Output\HTTP\Watchlist;

use N11t\Bundle\MovieBundle\Movie\View\MovieView;
use N11t\Bundle\MovieBundle\Watchlist\Output\WatchlistRemoveOutputInterface;

abstract class HttpWatchlistRemoveOutput implements WatchlistRemoveOutputInterface
{

    /**
     * @var int[]
     */
    public $notFoundIds = [];

    /**
     * @var MovieView[]
     */
    public $removedMovies = [];

    public function entryNotFoundForId(int $id): void
    {
        $this->notFoundIds[] = $id;
    }

    public function movieRemoved(MovieView $view): void
    {
        $this->removedMovies[] = $view;
    }
}
