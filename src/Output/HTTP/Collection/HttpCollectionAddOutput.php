<?php
declare(strict_types=1);

namespace App\Output\HTTP\Collection;

use N11t\Bundle\MovieBundle\Collection\Output\CollectionAddOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

abstract class HttpCollectionAddOutput implements CollectionAddOutputInterface
{

    /**
     * @var int[]
     */
    public $notFoundIds = [];

    /**
     * @var MovieView[]
     */
    public $moviesAdded = [];

    /**
     * @var MovieView[]
     */
    public $duplicateMovies = [];

    public function movieNotFound(int $id): void
    {
        $this->notFoundIds[] = $id;
    }

    public function movieAdded(MovieView $view): void
    {
        $this->moviesAdded[] = $view;
    }

    public function movieIsInCollection(MovieView $view): void
    {
        $this->duplicateMovies[] = $view;
    }
}
