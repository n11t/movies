<?php
declare(strict_types=1);

namespace App\Output\HTTP\Collection;

use N11t\Bundle\MovieBundle\Collection\Output\CollectionRemoveOutputInterface;
use N11t\Bundle\MovieBundle\Movie\View\MovieView;

abstract class HttpCollectionRemoveOutput implements CollectionRemoveOutputInterface
{

    /**
     * @var int[]
     */
    public $notFoundIds = [];

    /**
     * @var MovieView[]
     */
    public $removedMovies = [];

    public function entryNotFoundForId(int $id): void
    {
        $this->notFoundIds[] = $id;
    }

    public function movieRemoved(MovieView $view): void
    {
        $this->removedMovies[] = $view;
    }
}
