<?php
declare(strict_types=1);

namespace App\Output\HTTP\Collection;

use App\Output\HTTP\AbstractHttpListOutput;
use N11t\Bundle\MovieBundle\Collection\Output\CollectionListOutputInterface;
use N11t\Bundle\MovieBundle\Collection\View\CollectionEntryView;

abstract class HttpCollectionListOutput extends AbstractHttpListOutput implements CollectionListOutputInterface
{

    /**
     * @var CollectionEntryView[]
     */
    public $movies = [];

    public function addEntry(CollectionEntryView $view): void
    {
        $this->movies[] = $view;
    }
}
