<?php
declare(strict_types=1);

namespace App\Output\HTTP\Search;

use N11t\Bundle\MovieBundle\Search\Output\SearchOutputInterface;
use N11t\Bundle\MovieBundle\Search\View\SearchResultView;

abstract class HttpSearchOutput implements SearchOutputInterface
{

    /**
     * @var SearchResultView[]
     */
    public $results = [];

    /**
     * @param SearchResultView $view
     */
    public function addResult(SearchResultView $view): void
    {
        $this->results[] = $view;
    }
}
