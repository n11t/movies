<?php
declare(strict_types=1);

namespace App\Output\HTTP;

use N11t\Bundle\MovieBundle\Input\ListInputInterface;
use N11t\Bundle\MovieBundle\Output\ListOutputInterface;

abstract class AbstractHttpListOutput implements ListOutputInterface
{

    /**
     * @var ListInputInterface
     */
    protected $input;

    /**
     * @var int
     */
    protected $count;

    public function __construct(ListInputInterface $input)
    {
        $this->input = $input;
    }

    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    public function getPage(): int
    {
        return $this->input->getPage();
    }

    public function getMaxPage(): int
    {
        $limit = $this->getLimit();
        if (is_null($limit)) {
            return 1;
        }

        $maxPage = ceil($this->count / $limit);
        if ($maxPage < 1) {
            return 1;
        }

        return (int)$maxPage;
    }

    public function getLimit(): ?int
    {
        return $this->input->getLimit();
    }
}
