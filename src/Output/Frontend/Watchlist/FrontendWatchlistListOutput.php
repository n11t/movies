<?php
declare(strict_types=1);

namespace App\Output\Frontend\Watchlist;

use App\Output\HTTP\Watchlist\HttpWatchlistListOutput;

class FrontendWatchlistListOutput extends HttpWatchlistListOutput
{
}
