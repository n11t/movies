<?php
declare(strict_types=1);

namespace App\Output\Frontend\Collection;

use App\Output\HTTP\Collection\HttpCollectionListOutput;

class FrontendCollectionListOutput extends HttpCollectionListOutput
{
}
