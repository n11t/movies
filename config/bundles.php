<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\WebServerBundle\WebServerBundle::class => ['dev' => true],
    Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    N11t\Bundle\MovieAdapterBundle\N11tMovieAdapterBundle::class => ['all' => true],
    N11t\Bundle\MovieFakeAdapterBundle\N11tMovieFakeAdapterBundle::class => ['dev' => true],
    N11t\Bundle\MovieOMDBAdapterBundle\N11tMovieOMDBAdapterBundle::class => ['all' => true],
    N11t\Bundle\MovieBundle\N11tMovieBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
];
